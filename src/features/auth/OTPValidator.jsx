import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Typography, Row, Col } from 'antd';
import ReactCodesInput from 'react-codes-input';
import 'react-codes-input/lib/react-codes-input.min.css';
import { useDispatch } from 'react-redux';
import { sendRegisterCode } from './auth.slice';
import CenterText from 'app/layout/commons/CenterText';
import Clock from 'app/layout/commons/Clock';

OTPValidator.propTypes = {
  phone: PropTypes.string.isRequired,
  confirmOtpCode: PropTypes.func.isRequired,
  changePhoneNumber: PropTypes.func.isRequired,
  error: PropTypes.string,
};

OTPValidator.defaultProps = {
  phone: '123456789',
};

const { Text } = Typography;

function OTPValidator({ phone, confirmOtpCode, changePhoneNumber, error }) {
  const dispatch = useDispatch();
  const [form] = Form.useForm();
  const [otp, setOtp] = useState('');

  const handleConfirm = () => confirmOtpCode(otp);

  const handleSendRegisterCode = () => dispatch(sendRegisterCode(phone));

  const goBack = () => changePhoneNumber();

  return (
    <Form form={form} size='large'>
      <Form.Item>
        <ReactCodesInput
          initialFocus={false}
          codeLength={4}
          type='number'
          hide={false}
          value={otp}
          onChange={res => setOtp(res)}
          customStyleComponent={{
            maxWidth: '300px',
            margin: '0 auto',
          }}
        />
      </Form.Item>

      <Form.Item>
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
          }}
        >
          <Text>
            Hãy kiểm tra tin nhắn, chúng tôi vừa gửi OTP đến số{' '}
            <Text strong>{phone}</Text>
          </Text>
          <Text>
            Yêu cầu gửi lại sau <Clock initTime={10} callback={goBack} />
          </Text>
        </div>
      </Form.Item>
      {error && <CenterText text={error} />}
      <Form.Item
        help={
          <Button type='link' block onClick={goBack}>
            <Text type='danger' style={{ fontSize: 11 }}>
              Thay đổi số điện thoại
            </Text>
          </Button>
        }
        style={{ marginTop: 64 }}
      >
        <Row gutter={12}>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Button
              type='default'
              onClick={handleSendRegisterCode}
              size='large'
              block
            >
              Gửi lại
            </Button>
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Button type='primary' size='large' block onClick={handleConfirm}>
              Xác nhận
            </Button>
          </Col>
        </Row>
        <div style={{ display: 'flex', justifyContent: 'space-between' }}></div>
      </Form.Item>
    </Form>
  );
}

export default OTPValidator;
