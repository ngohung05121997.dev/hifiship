import React, { useEffect } from 'react';
import './auth.style.css';
import { Layout, Space } from 'antd';
import { useDispatch } from 'react-redux';
import { asyncActionFinish } from 'features/async/async.slice';
import logo1 from 'assets/images/logo/logo1.png';
import logo2 from 'assets/images/logo/logo2.png';

const AuthLayout = ({ children, header, requireLogo = true }) => {
  const dispatch = useDispatch();
  useEffect(() => {
    return () => {
      dispatch(asyncActionFinish());
    };

    // eslint-disable-next-line
  }, []);

  return (
    <Layout className='auth-layout'>
      <div className='auth-form'>
        {requireLogo && (
          <div className='auth-logo'>
            <div className='logo1'>
              <img src={logo1} alt='logo1' />
            </div>
            <div className='logo2'>
              <img src={logo2} alt='logo2' />
            </div>
          </div>
        )}
        <Space
          style={{
            marginBottom: 24,
            fontSize: 18,
            fontWeight: 500,
            color: '#000',
          }}
        >
          {header}
        </Space>
        {children}
      </div>
    </Layout>
  );
};

export default AuthLayout;
