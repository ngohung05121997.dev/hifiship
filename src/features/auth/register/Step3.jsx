import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Button } from 'antd';
import AuthLayout from '../AuthLayout';
import { UserIcon } from 'app/layout/commons/Icons';
import {
  MailOutlined,
  MobileOutlined,
  LockFilled,
  RedoOutlined,
} from '@ant-design/icons';
import CenterText from 'app/layout/commons/CenterText';

Step3.propTypes = {
  credentials: PropTypes.object.isRequired,
  error: PropTypes.string,
  register: PropTypes.func.isRequired,
  registerLoading: PropTypes.bool.isRequired,
};

Step3.defaultProps = {
  credentials: {
    name: 'Admin',
    email: 'admin@example.com',
    phone: '0123456789',
  },
  registerLoading: false,
};

const { Password } = Input;

function Step3({ credentials, error, register, registerLoading }) {
  const [form] = Form.useForm();

  const handleSubmit = values => register({ ...credentials, ...values });

  useEffect(() => {
    form.setFieldsValue(credentials);

    return () => {
      form.resetFields();
    };

    // eslint-disable-next-line
  }, []);

  const confirmPasswordValidator = (rule, value) => {
    const currentPassword = form.getFieldValue('password');

    if (value !== currentPassword) {
      return Promise.reject('Mật khẩu không khớp');
    }

    return Promise.resolve();
  };

  return (
    <AuthLayout header={<span>Cập nhật mật khẩu</span>}>
      <Form form={form} size='large' onFinish={handleSubmit}>
        <Form.Item name='name'>
          <Input placeholder='' disabled prefix={<UserIcon />} />
        </Form.Item>
        <Form.Item name='email'>
          <Input
            placeholder=''
            disabled
            prefix={<MailOutlined className='primary-color' />}
          />
        </Form.Item>
        <Form.Item name='phone'>
          <Input
            placeholder=''
            disabled
            prefix={<MobileOutlined className='primary-color' />}
          />
        </Form.Item>
        <Form.Item
          name='password'
          rules={[
            { required: true, message: 'Vui lòng nhập mật khẩu của bạn' },
          ]}
        >
          <Password
            placeholder='Mật khẩu'
            prefix={<LockFilled className='primary-color' />}
          />
        </Form.Item>
        <Form.Item
          name='confirmPassword'
          rules={[{ validator: confirmPasswordValidator }]}
        >
          <Password
            placeholder='Nhập lại mật khẩu'
            prefix={<RedoOutlined className='primary-color' />}
          />
        </Form.Item>
        {error && (
          <Form.Item>
            <CenterText text={error} />
          </Form.Item>
        )}
        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            size='large'
            block
            loading={registerLoading}
          >
            Cập nhật
          </Button>
        </Form.Item>
      </Form>
    </AuthLayout>
  );
}

export default Step3;
