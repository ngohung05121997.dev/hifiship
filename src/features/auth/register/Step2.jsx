import React from 'react';
import PropTypes from 'prop-types';
import AuthLayout from '../AuthLayout';
import OTPValidator from '../OTPValidator';

Step2.propTypes = {
  phone: PropTypes.string.isRequired,
  confirmOtpCode: PropTypes.func.isRequired,
  changePhoneNumber: PropTypes.func.isRequired,
  error: PropTypes.string,
};

function Step2({ phone, confirmOtpCode, changePhoneNumber, error }) {
  return (
    <AuthLayout header={<span>Mã OTP của tôi</span>}>
      <OTPValidator
        phone={phone}
        confirmOtpCode={confirmOtpCode}
        changePhoneNumber={changePhoneNumber}
        error={error}
      />
    </AuthLayout>
  );
}

export default Step2;
