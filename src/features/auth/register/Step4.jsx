import React from 'react';
import { Link } from 'react-router-dom';
import { Result, Button, Typography } from 'antd';
import { CheckCircleOutlined } from '@ant-design/icons';
import { pages } from 'app/utils/config';

const { Text } = Typography;

Step4.propTypes = {};

function Step4(props) {
  return (
    <Result
      icon={<CheckCircleOutlined style={{ color: '#52C41A' }} />}
      title={
        <Text type='danger' style={{ textTransform: 'uppercase' }}>
          ĐĂNG KÝ THÀNH CÔNG
        </Text>
      }
      subTitle={
        <div
          style={{
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
            flexDirection: 'column',
          }}
        >
          <Text>Bạn đã đăng ký tài khoản thành công</Text>
          <Text>
            Bạn có thể cập nhật và chỉnh sửa thông tin trong phần thông tin tài
            khoản
          </Text>
        </div>
      }
      extra={[
        <Button type='primary' key='login'>
          <Link to={pages.LOGIN.path}>Đăng nhập</Link>
        </Button>,
      ]}
    />
  );
}

export default Step4;
