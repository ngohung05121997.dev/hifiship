import React, { Fragment } from 'react';
import { Link, NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Form, Input, Button, Typography, Row, Col } from 'antd';
import { MobileOutlined } from '@ant-design/icons';
import AuthLayout from '../AuthLayout';
import CenterText from 'app/layout/commons/CenterText';
import { pages } from 'app/utils/config';

Step1.propTypes = {
  validatePhoneNumber: PropTypes.func.isRequired,
  validatePhoneNumberLoading: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

Step1.defaultProps = {
  validatePhoneNumberLoading: false,
};

const { Text } = Typography;

const registerHeader = (
  <Fragment>
    <NavLink
      activeClassName='auth--active'
      className='auth-action'
      to={pages.LOGIN.path}
    >
      Đăng nhập
    </NavLink>
    <NavLink
      activeClassName='auth--active'
      className='auth-action'
      to={pages.REGISTER.path}
    >
      Đăng ký
    </NavLink>
  </Fragment>
);

function Step1({ validatePhoneNumber, validatePhoneNumberLoading, error }) {
  const [form] = Form.useForm();

  const handleSubmit = credentials => {
    validatePhoneNumber(credentials);
  };

  return (
    <AuthLayout header={registerHeader}>
      <Form form={form} size='large' onFinish={handleSubmit}>
        <Row gutter={12}>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Form.Item
              name='name'
              rules={[{ required: true, message: 'Vui lòng nhập tên của bạn' }]}
            >
              <Input placeholder='Họ và tên' />
            </Form.Item>
          </Col>
          <Col xs={24} sm={24} md={12} lg={12} xl={12}>
            <Form.Item
              name='email'
              rules={[
                { required: true, message: 'Vui lòng nhập email của bạn' },
                { type: 'email', message: 'Vui lòng nhập một email hợp lệ' },
              ]}
            >
              <Input placeholder='Email' />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item
          name='phone'
          rules={[
            { required: true, message: 'Vui lòng nhập số điện thoại của bạn' },
          ]}
        >
          <Input
            placeholder='Số điện thoại'
            prefix={<MobileOutlined className='primary-color' />}
          />
        </Form.Item>
        <Form.Item name='push_token'>
          <Input placeholder='Mã giới thiệu (không bắt buộc)' />
        </Form.Item>
        {error && (
          <Form.Item>
            <CenterText text={error} />
          </Form.Item>
        )}
        <Form.Item
          help={
            <Text
              style={{ display: 'block', textAlign: 'center', width: '100%' }}
            >
              Bằng việc đăng ký, bạn đã đồng ý với{' '}
              <Link to={pages.POLICY.path}>chính sách của Hifiship</Link>
            </Text>
          }
        >
          <Button
            type='primary'
            htmlType='submit'
            block
            size='large'
            loading={validatePhoneNumberLoading}
          >
            Đăng ký
          </Button>
        </Form.Item>
      </Form>
    </AuthLayout>
  );
}

export default Step1;
