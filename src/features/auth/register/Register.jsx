import React, { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import { loadingTypes } from 'app/utils/config';
import { sendRegisterCode, register } from '../auth.slice';
import { useHistory } from 'react-router-dom';
import useAsync from 'app/hooks/useAsync';

Register.propTypes = {};

const { VALIDATE_PHONE_NUMBER, REGISTER } = loadingTypes.auth;

const credentialsInitState = {
  phone: '',
  name: '',
  email: '',
  is_shipper: true,
  token: '',
  password: '',
  push_token: '',
};

function Register(props) {
  const dispatch = useDispatch();
  const history = useHistory();

  const [step, setStep] = useState(1);
  const [otpError, setOtpError] = useState(null);
  const [credentials, setCredentials] = useState(credentialsInitState);

  const { error } = useAsync();
  const { loading: validatePhoneNumberLoading } = useAsync(
    VALIDATE_PHONE_NUMBER
  );
  const { loading: registerLoading } = useAsync(REGISTER);

  useEffect(() => {
    return () => {
      setCredentials(credentialsInitState);
      setOtpError(null);
      setStep(1);
    };
  }, []);

  const handleValidatePhoneNumber = async ({
    name,
    email,
    phone,
    push_token,
  }) => {
    const phoneValid = await dispatch(sendRegisterCode(phone));

    if (phoneValid) {
      setCredentials({ ...credentials, name, email, phone, push_token });
      setStep(2);
    }
  };

  const goBack = () => setStep(1);

  const handleConfirmOtpCode = otp => {
    if (otp.length === 4) {
      setCredentials({ ...credentials, token: otp });
      setStep(3);
    } else {
      setOtpError('Vui lòng nhập mã OTP');
    }
  };

  const handleRegister = async credentials => {
    await dispatch(register(credentials, history));
  };

  const renderRegisterContent = () => {
    let content = '';
    switch (step) {
      case 1:
        content = (
          <Step1
            validatePhoneNumberLoading={validatePhoneNumberLoading}
            validatePhoneNumber={handleValidatePhoneNumber}
            error={error}
          />
        );
        break;
      case 2:
        content = (
          <Step2
            phone={credentials.phone}
            confirmOtpCode={handleConfirmOtpCode}
            changePhoneNumber={goBack}
            error={otpError}
          />
        );
        break;
      case 3:
        content = (
          <Step3
            credentials={credentials}
            error={error}
            register={handleRegister}
            registerLoading={registerLoading}
          />
        );
        break;
      default:
        content = (
          <Step1
            validatePhoneNumberLoading={validatePhoneNumberLoading}
            validatePhoneNumber={handleValidatePhoneNumber}
            error={error}
          />
        );
        break;
    }

    return content;
  };

  // const validatePhoneNumberLoading =
  //   loadingType === authLoadingTypes.VALIDATE_PHONE_NUMBER ? loading : false;
  // const registerLoading = authLoadingTypes.REGISTER ? loading : false;

  return <span>{renderRegisterContent()}</span>;
}

export default Register;
