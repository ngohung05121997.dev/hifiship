import { createSlice } from '@reduxjs/toolkit';

import { loadingTypes } from 'app/utils/config';
import { isAuthenticated } from 'app/utils/helper';
import authApi from 'api/auth.api';
import {
  asyncActionStart,
  asyncActionError,
  asyncActionFinish,
} from 'features/async/async.slice';
import { openModal } from 'features/modal/modal.slice';

const token = isAuthenticated();
const authLoadingTypes = loadingTypes.auth;

const initialState = {
  authenticated: token ? true : false,
  user: {
    data: {},
    balance: 0,
  },
};

const authSlice = createSlice({
  name: 'auth',
  initialState: initialState,
  reducers: {
    setAuth: (state, { payload: { data, balance } }) => {
      state.authenticated = true;
      state.user = { data, balance };
    },
    setUnAuth: state => {
      localStorage.removeItem('token');
      state.authenticated = false;
      state.user = {
        data: {},
        balance: 0,
      };
    },
  },
});

const { setAuth, setUnAuth } = authSlice.actions;

const getAuthBalance = async () => {
  try {
    const res = await authApi.getAuthBalance();

    if (res.status === 'fail') {
      return 0;
    }

    return res.data;
  } catch (err) {
    console.log(err);
    return 0;
  }
};

const login = (userCredentials, history) => async dispatch => {
  dispatch(asyncActionStart({ loadingType: authLoadingTypes.LOGIN }));

  try {
    const res = await authApi.login(userCredentials);

    if (res.status === 'success') {
      const { token, ...data } = res.data;
      const balance = await getAuthBalance();

      dispatch(setAuth({ data, balance }));
      dispatch(asyncActionFinish());
      localStorage.setItem('token', token);
      history.push('/main');
    } else {
      dispatch(asyncActionError({ error: res.data }));
    }
  } catch (err) {
    console.log(err);
    dispatch(asyncActionError({ error: err }));
  }
};

const register = (userCredentials, history) => async dispatch => {
  dispatch(asyncActionStart({ loadingType: authLoadingTypes.REGISTER }));

  try {
    const res = await authApi.register(userCredentials);

    if (res.status === 'success') {
      const { data } = res;
      const balance = await getAuthBalance();

      dispatch(setAuth({ data, balance }));
      dispatch(asyncActionFinish());
      dispatch(openModal({ modalType: 'RegisterSuccess' }));
      history.push('/main');

      return true;
    } else {
      dispatch(asyncActionError({ error: res.data }));

      return false;
    }
  } catch (err) {
    console.log(err);
    dispatch(asyncActionError({ error: err }));

    return false;
  }
};

const logout = () => dispatch => dispatch(setUnAuth());

const getAuthUser = () => async dispatch => {
  dispatch(asyncActionStart({ loadingType: authLoadingTypes.GET_AUTH_USER }));

  try {
    const res = await authApi.getAuthUser();

    if (res.status === 'success') {
      const { data } = res;
      const balance = await getAuthBalance();

      dispatch(setAuth({ data, balance }));
      dispatch(asyncActionFinish());
    } else {
      dispatch(asyncActionError({ error: res.data }));
    }
  } catch (err) {
    console.log(err);
    dispatch(asyncActionError({ error: err }));
  }
};

const checkPhone = phone => async dispatch => {
  dispatch(asyncActionStart({ loadingType: authLoadingTypes.CHECK_PHONE }));

  try {
    const res = await authApi.checkPhone(phone);

    if (res.status === 'success') {
      dispatch(asyncActionFinish());

      return true;
    } else {
      dispatch(asyncActionError({ error: res.data }));
      return false;
    }
  } catch (err) {
    console.log(err);
    dispatch(asyncActionError({ error: err }));

    return false;
  }
};

const sendRegisterCode = phone => async dispatch => {
  dispatch(
    asyncActionStart({ loadingType: authLoadingTypes.VALIDATE_PHONE_NUMBER })
  );

  try {
    const res = await authApi.sendRegisterCode(phone);

    if (res.status === 'success') {
      dispatch(asyncActionFinish());

      return true;
    } else {
      dispatch(asyncActionError({ error: res.data }));
      return false;
    }
  } catch (err) {
    console.log(err);
    dispatch(asyncActionError({ error: err }));

    return false;
  }
};

const checkOtpCode = code => async dispatch => {
  dispatch(asyncActionStart({ loadingType: authLoadingTypes.CHECK_OTP_CODE }));

  try {
    const res = await authApi.checkOtp(code);

    if (res.status === 'success') {
      dispatch(asyncActionFinish());

      return true;
    } else {
      dispatch(asyncActionError({ error: res.data }));

      return false;
    }
  } catch (err) {
    console.log(err);
    dispatch(asyncActionError({ error: err }));

    return false;
  }
};

export default authSlice.reducer;

export {
  login,
  register,
  logout,
  getAuthUser,
  checkPhone,
  sendRegisterCode,
  checkOtpCode,
};
