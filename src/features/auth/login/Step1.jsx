import React from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Button } from 'antd';
import { MobileOutlined } from '@ant-design/icons';
import CenterText from 'app/layout/commons/CenterText';

Step1.propTypes = {
  checkPhone: PropTypes.func.isRequired,
  checkPhoneLoading: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

function Step1({ checkPhone, checkPhoneLoading, error }) {
  const [form] = Form.useForm();
  const handleSubmit = ({ phone }) => checkPhone(phone);

  return (
    <Form onFinish={handleSubmit} size='large' form={form}>
      <Form.Item
        name='phone'
        rules={[
          { required: true, message: 'Vui lòng nhập số điện thoại của bạn' },
        ]}
      >
        <Input
          prefix={<MobileOutlined className='primary-color' />}
          placeholder='Số điện thoại của bạn'
        />
      </Form.Item>
      {error && (
        <Form.Item>
          <CenterText text={error} />
        </Form.Item>
      )}
      <Form.Item>
        <Button
          type='primary'
          htmlType='submit'
          block
          size='large'
          loading={checkPhoneLoading}
        >
          Đăng nhập
        </Button>
      </Form.Item>
    </Form>
  );
}

export default Step1;
