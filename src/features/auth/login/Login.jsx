import React, { Fragment, useState } from 'react';
import './login.style.css';
import { NavLink, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Button, Divider } from 'antd';
import { FacebookOutlined } from '@ant-design/icons';
import { GoogleIcon } from 'app/layout/commons/Icons';
import AuthLayout from '../AuthLayout';
import Step1 from './Step1';
import Step2 from './Step2';
import { checkPhone, login } from '../auth.slice';
import { loadingTypes, pages } from 'app/utils/config';
import useAsync from 'app/hooks/useAsync';

Login.propTypes = {};

const { CHECK_PHONE, LOGIN } = loadingTypes.auth;

const loginHeader = (
  <Fragment>
    <NavLink
      activeClassName='auth--active'
      className='auth-action'
      to={pages.LOGIN.path}
    >
      Đăng nhập
    </NavLink>
    <NavLink
      activeClassName='auth--active'
      className='auth-action'
      to={pages.REGISTER.path}
    >
      Đăng ký
    </NavLink>
  </Fragment>
);

function Login(props) {
  const history = useHistory();
  const dispatch = useDispatch();

  const [step, setStep] = useState(1);
  const [credential, setCredential] = useState({
    phone: '',
    password: '',
  });

  const { error } = useAsync();
  const { loading: checkPhoneLoading } = useAsync(CHECK_PHONE);
  const { loading: loginLoading } = useAsync(LOGIN);

  const handleCheckPhone = async phone => {
    const phoneValid = await dispatch(checkPhone(phone));

    if (phoneValid) {
      setStep(2);
      setCredential({ ...credential, phone });
    }
  };

  const goBack = () => setStep(1);

  const handleLogin = password => {
    dispatch(
      login(
        {
          ...credential,
          password,
          app_type: 'customer',
          push_token: 'random_string',
        },
        history
      )
    );
  };

  const renderLoginContent = () => {
    let content = '';
    switch (step) {
      case 1:
        content = (
          <Step1
            checkPhone={handleCheckPhone}
            checkPhoneLoading={checkPhoneLoading}
            error={error}
          />
        );
        break;
      case 2:
        content = (
          <Step2
            loginLoading={loginLoading}
            phone={credential.phone}
            login={handleLogin}
            goBack={goBack}
            error={error}
          />
        );
        break;
      default:
        content = (
          <Step1
            checkPhone={handleCheckPhone}
            checkPhoneLoading={checkPhoneLoading}
            error={error}
          />
        );
        break;
    }

    return content;
  };

  return (
    <AuthLayout header={loginHeader}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <Button
          className='btn btn--facebook'
          icon={<FacebookOutlined style={{ fontSize: 18 }} />}
          size='large'
        >
          Facebook
        </Button>
        <Button
          className='btn btn--google'
          type='default'
          icon={<GoogleIcon />}
          size='large'
        >
          Google
        </Button>
      </div>
      <Divider />
      {renderLoginContent()}
    </AuthLayout>
  );
}

export default Login;
