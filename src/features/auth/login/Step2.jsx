import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Input, Typography, Tag } from 'antd';
import { LockFilled } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { pages } from 'app/utils/config';
import CenterText from 'app/layout/commons/CenterText';

Step2.propTypes = {
  phone: PropTypes.string.isRequired,
  loginLoading: PropTypes.bool.isRequired,
  login: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  error: PropTypes.string,
};

Step2.defaultProps = {
  phone: '123456789',
  loginLoading: false,
};

const { Text } = Typography;

function Step2({ phone, loginLoading, login, goBack, error }) {
  const [form] = Form.useForm();

  const handleSubmit = ({ password }) => login(password);

  return (
    <Form size='large' form={form} onFinish={handleSubmit}>
      <Form.Item>
        <Text>
          Đăng nhập với tài khoản{' '}
          <Tag color='geekblue' closable onClose={() => goBack()}>
            {phone}
          </Tag>
        </Text>
      </Form.Item>
      <Form.Item
        name='password'
        rules={[{ required: true, message: 'Vui lòng nhập mật khẩu của bạn' }]}
      >
        <Input.Password
          placeholder='Mật khẩu'
          prefix={<LockFilled className='primary-color' />}
        />
      </Form.Item>
      {error && (
        <Form.Item>
          <CenterText text={error} />
        </Form.Item>
      )}
      <Form.Item
        help={
          <Link to={pages.FORGET_PASSWORD.path}>
            <CenterText text='Quên mật khẩu' />
          </Link>
        }
      >
        <Button
          type='primary'
          htmlType='submit'
          block
          size='large'
          loading={loginLoading}
        >
          Đăng nhập
        </Button>
      </Form.Item>
    </Form>
  );
}

export default Step2;
