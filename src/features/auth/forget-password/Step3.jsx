import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Input } from 'antd';
import { LockFilled, RedoOutlined } from '@ant-design/icons';
import AuthLayout from '../AuthLayout';
import CenterText from 'app/layout/commons/CenterText';

Step3.propTypes = {
  updatePassword: PropTypes.func.isRequired,
  updatePasswordLoading: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

Step3.defaultProps = {
  updatePasswordLoading: false,
};

const { Password } = Input;

function Step3({ updatePassword, updatePasswordLoading, error }) {
  const [form] = Form.useForm();

  const handleSubmit = ({ password }) => updatePassword(password);

  const confirmPasswordValidator = (rule, value) => {
    const currentPassword = form.getFieldValue('password');

    if (value !== currentPassword) {
      return Promise.reject('Mật khẩu không khớp');
    }

    return Promise.resolve();
  };

  return (
    <AuthLayout header={<span>Tạo mật khẩu</span>}>
      <Form form={form} size='large' onFinish={handleSubmit}>
        <Form.Item
          name='password'
          rules={[{ required: true, message: 'Vui lòng nhập mật khẩu mới' }]}
        >
          <Password
            prefix={<LockFilled className='primary-color' />}
            placeholder='Mật khẩu'
          />
        </Form.Item>
        <Form.Item
          name='confirmPassword'
          rules={[{ validator: confirmPasswordValidator }]}
        >
          <Password
            prefix={<RedoOutlined className='primary-color' />}
            placeholder='Nhập lại mật khẩu'
          />
        </Form.Item>
        {error && (
          <Form.Item>
            <CenterText text={error} />
          </Form.Item>
        )}
        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            size='large'
            block
            loading={updatePasswordLoading}
          >
            Cập nhật mật khẩu
          </Button>
        </Form.Item>
      </Form>
    </AuthLayout>
  );
}

export default Step3;
