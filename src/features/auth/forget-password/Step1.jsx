import React from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Input } from 'antd';
import { MobileOutlined } from '@ant-design/icons';
import AuthLayout from '../AuthLayout';
import CenterText from 'app/layout/commons/CenterText';

Step1.propTypes = {
  sendOtpCode: PropTypes.func.isRequired,
  sendOtpCodeLoading: PropTypes.bool.isRequired,
  error: PropTypes.string,
};

Step1.defaultProps = {
  sendOtpCodeLoading: false,
};

function Step1({ sendOtpCode, sendOtpCodeLoading, error }) {
  const [form] = Form.useForm();

  const handleSubmit = ({ phone }) => sendOtpCode(phone);

  return (
    <AuthLayout header={<span>Quên mật khẩu</span>}>
      <Form form={form} size='large' onFinish={handleSubmit}>
        <Form.Item
          name='phone'
          rules={[{ required: true, message: 'Vui lòng nhập số điện thoại' }]}
        >
          <Input
            prefix={<MobileOutlined className='primary-color' />}
            placeholder='Nhập số điện thoại'
          />
        </Form.Item>
        {error && (
          <Form.Item>
            <CenterText text={error} />
          </Form.Item>
        )}
        <Form.Item>
          <Button
            type='primary'
            htmlType='submit'
            block
            size='large'
            loading={sendOtpCodeLoading}
          >
            Gửi mã
          </Button>
        </Form.Item>
      </Form>
    </AuthLayout>
  );
}

export default Step1;
