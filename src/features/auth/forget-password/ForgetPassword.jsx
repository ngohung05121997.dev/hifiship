import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';
import { loadingTypes } from 'app/utils/config';
import { checkPhone } from '../auth.slice';

ForgetPassword.propTypes = {};

const authLoadingTypes = loadingTypes.auth;

function ForgetPassword(props) {
  const dispatch = useDispatch();
  const [step, setStep] = useState(1);
  const [otpError, setOtpError] = useState(null);
  const [credentials, setCredentials] = useState({
    phone: '',
    token: '',
    newPassword: '',
  });
  const { loading, loadingType, error } = useSelector(state => state.async);

  const handleSendOtpCode = async phone => {
    const phoneValid = await dispatch(checkPhone(phone));

    if (phoneValid) {
      setStep(2);
      setCredentials({ ...credentials, phone });
    }
  };

  const handleConfirmOtp = otp => {
    if (otp.length === 4) {
      setCredentials({ ...credentials, token: otp });
      setStep(3);
    } else {
      setOtpError('Vui lòng nhập mã OTP');
    }
  };

  const handleUpdatePassword = password => {};

  const changePhoneNumber = () => setStep(1);

  const renderForgetPasswordContent = () => {
    let content = '';

    switch (step) {
      case 1:
        content = (
          <Step1
            sendOtpCode={handleSendOtpCode}
            sendOtpCodeLoading={sendOtpCodeLoading}
            error={error}
          />
        );
        break;
      case 2:
        content = (
          <Step2
            phone={credentials.phone}
            confirmOtpCode={handleConfirmOtp}
            changePhoneNumber={changePhoneNumber}
            error={otpError}
          />
        );
        break;
      case 3:
        content = (
          <Step3
            updatePassword={handleUpdatePassword}
            updatePasswordLoading={updatePasswordLoading}
            error={error}
          />
        );
        break;
      default:
        content = (
          <Step1
            sendOtpCode={handleSendOtpCode}
            sendOtpCodeLoading={sendOtpCodeLoading}
            error={error}
          />
        );
        break;
    }

    return content;
  };

  const sendOtpCodeLoading =
    loadingType === authLoadingTypes.SEND_OTP_CODE ? loading : false;

  const updatePasswordLoading =
    loadingType === authLoadingTypes.UPDATE_PASSWORD ? loading : false;

  return <span>{renderForgetPasswordContent()}</span>;
}

export default ForgetPassword;
