import { createSlice } from '@reduxjs/toolkit';

export const asyncSlice = createSlice({
  name: 'async',
  initialState: {
    loading: false,
    loadingType: null,
    loadingElement: null,
    error: null,
  },
  reducers: {
    asyncActionStart: (state, action) => {
      const { loadingType, loadingElement } = action.payload;

      state.loading = true;
      if (loadingType) {
        state.loadingType = loadingType;
      }
      if (loadingElement) {
        state.loadingElement = loadingElement;
      }
    },
    asyncActionError: (state, action) => {
      state.loading = false;
      state.loadingType = null;
      state.loadingElement = null;

      if (action.payload.error) {
        state.error = action.payload.error;
      }
    },
    asyncActionFinish: state => {
      state.loading = false;
      state.loadingType = null;
      state.loadingElement = null;
      state.error = null;
    },
  },
});

export const {
  asyncActionStart,
  asyncActionFinish,
  asyncActionError,
} = asyncSlice.actions;

// The function below is called a selector and allows us to select a value from
// the state. Selectors can also be defined inline where they're used instead of
// in the slice file. For example: `useSelector((state) => state.counter.value)`
// export const selectCount = state => state.counter.value;

export default asyncSlice.reducer;
