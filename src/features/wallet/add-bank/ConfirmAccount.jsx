import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Typography, Button, Form, Row, Col } from 'antd';
import ReactCodesInput from 'react-codes-input';
import Clock from 'app/layout/commons/Clock';

const { Text } = Typography;

ConfirmAccount.propTypes = {
  phone: PropTypes.string.isRequired,
  cancel: PropTypes.func.isRequired,
  confirm: PropTypes.func.isRequired,
};

ConfirmAccount.defaultProps = {
  phone: '+84966382597',
};

function ConfirmAccount({ phone, cancel, confirm }) {
  const [otp, setOtp] = useState();

  const [form] = Form.useForm();

  return (
    <div>
      <Form form={form} size='large'>
        <Form.Item>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'center',
              justifyContent: 'flex-start',
            }}
          >
            <Text style={{ textAlign: 'center' }}>
              Hãy kiểm tra tin nhắn, chúng tôi vừa gửi OTP đến số
            </Text>
            <Text strong>{phone}</Text>
          </div>
        </Form.Item>
        <Form.Item>
          <ReactCodesInput
            initialFocus={false}
            codeLength={4}
            type='number'
            hide={false}
            value={otp}
            onChange={res => setOtp(res)}
            customStyleComponent={{
              maxWidth: '300px',
              margin: '0 auto',
            }}
          />
        </Form.Item>

        <Form.Item>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
            }}
          >
            <Clock
              initTime={120}
              style={{
                color: 'rgb(250, 140, 22)',
                width: '100%',
                textAlign: 'center',
              }}
            />
          </div>
        </Form.Item>
        <Form.Item>
          <Row gutter={12}>
            <Col xs={24} sm={24} md={12} lg={12} xl={12}>
              <Button
                type='default'
                size='large'
                block
                className='btn--default'
                onClick={cancel}
              >
                Hủy bỏ
              </Button>
            </Col>
            <Col xs={24} sm={24} md={12} lg={12} xl={12}>
              <Button type='primary' size='large' block onClick={confirm}>
                Xác nhận
              </Button>
            </Col>
          </Row>
        </Form.Item>
      </Form>
    </div>
  );
}

export default ConfirmAccount;
