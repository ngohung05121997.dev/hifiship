import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Typography, List, Avatar, Button, Space } from 'antd';
import { InfoCircleFilled } from '@ant-design/icons';
import AddBankLogo from 'assets/images/bank/add-bank-logo.png';
import CashWithdrawalSupportLogo from 'assets/images/bank/cash-withdrawal-support.png';
import FreeTransactionLogo from 'assets/images/bank/free-transaction.png';
import GiftLogo from 'assets/images/bank/gift.png';
import { openModal } from 'features/modal/modal.slice';

const { Text } = Typography;

AddBankInfo.propTypes = {
  cancel: PropTypes.func.isRequired,
};

function AddBankInfo({ cancel }) {
  const dispatch = useDispatch();

  const handleAddBankForm = () =>
    dispatch(openModal({ modalType: 'AddBankFormModal' }));

  return (
    <Fragment>
      <div>
        <img alt='header' src={AddBankLogo} style={{ width: 100 }} />
      </div>

      <Text strong style={{ color: '#111', width: '80%', textAlign: 'center' }}>
        Thêm tài khoản ngân hàng để nhận được những lợi ích sau
      </Text>

      <List style={{ width: '100%' }}>
        <List.Item style={{ padding: '6px 0', border: 'none' }}>
          <List.Item.Meta
            avatar={
              <Avatar
                src={CashWithdrawalSupportLogo}
                alt='cash-withdrawal-support'
              />
            }
            title={<Text style={{ fontSize: 13 }}>Hỗ trợ rút tiền</Text>}
            description={
              <Text style={{ fontSize: 13 }}>
                Rút trực tiếp từ tài khoản COD về tài khoản ngân hàng
              </Text>
            }
          />
        </List.Item>
        <List.Item style={{ padding: '6px 0', border: 'none' }}>
          <List.Item.Meta
            avatar={<Avatar src={FreeTransactionLogo} alt='free-transaction' />}
            title={<Text style={{ fontSize: 13 }}>Miễn phí giao dịch</Text>}
            description={
              <Text style={{ fontSize: 13 }}>Hỗ trợ rút tiền miễn phí</Text>
            }
          />
        </List.Item>
        <List.Item style={{ padding: '6px 0', border: 'none' }}>
          <List.Item.Meta
            avatar={<Avatar src={GiftLogo} alt='gift' />}
            title={<Text style={{ fontSize: 13 }}>Nhận nhiều ưu đãi</Text>}
            description={
              <Text style={{ fontSize: 13 }}>
                Các chương trình ưu đãi hấp dẫn khi thanh toán không dùng tiền
                mặt
              </Text>
            }
          />
        </List.Item>
      </List>

      <div
        style={{
          display: 'flex',
          alignItems: 'flex-start',
          padding: 8,
          backgroundColor: 'rgb(234 231 231)',
          borderRadius: 4,
          marginBottom: 24,
        }}
      >
        <InfoCircleFilled />
        <Text style={{ fontSize: 12, lineHeight: '12px', marginLeft: 4 }}>
          Sử dụng tài khoản ngân hàng để rút tiền COD của dịch vụ Giao Hàng Liên
          Tỉnh
        </Text>
      </div>

      <Space direction='vertical' style={{ width: '100%' }}>
        <Button
          block
          type='primary'
          className='btn--warning'
          onClick={handleAddBankForm}
        >
          Thêm tài khoản
        </Button>
        <Button block type='primary' className='btn--default' onClick={cancel}>
          Để sau
        </Button>
      </Space>
    </Fragment>
  );
}

export default AddBankInfo;
