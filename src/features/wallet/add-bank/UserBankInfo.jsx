import React, { Fragment } from 'react';
import './bank.style.css';
import PropTypes from 'prop-types';
import {
  Card,
  List,
  Avatar,
  Typography,
  Space,
  Button,
  Menu,
  Dropdown,
} from 'antd';
import { InfoCircleFilled, EditFilled } from '@ant-design/icons';
import CashWithdrawalSupportLogo from 'assets/images/bank/cash-withdrawal-support.png';
import FreeTransactionLogo from 'assets/images/bank/free-transaction.png';
import GiftLogo from 'assets/images/bank/gift.png';
import UserBankInfoImage from 'assets/images/bank/user-bank-info.png';

const { Text } = Typography;

UserBankInfo.propTypes = {
  cancel: PropTypes.func.isRequired,
};

function UserBankInfo({ cancel }) {
  const menu = (
    <Menu>
      <Menu.Item key='Chỉnh sửa' style={{ paddingTop: 2, paddingBottom: 2 }}>
        <Text>Chỉnh sửa</Text>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item
        key='Xóa tài khoản'
        style={{ paddingTop: 2, paddingBottom: 2 }}
      >
        <Text type='warning'>Xóa tài khoản</Text>
      </Menu.Item>
    </Menu>
  );

  return (
    <Fragment>
      <Card
        bodyStyle={{
          padding: '36px 12px 24px 12px',
          backgroundColor: '#ccc',
          position: 'relative',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'flex-end',
          alignItems: 'flex-start',
          borderRadius: 6,
          overflow: 'hidden',
          background: `url(${UserBankInfoImage})`,
          backgroundSize: 'auto',
          backgroundPosition: 'center center',
        }}
        style={{ marginBottom: 24 }}
      >
        <div
          style={{ display: 'flex', flexDirection: 'column', marginBottom: 16 }}
        >
          <Text style={{ color: '#fff', fontSize: 12, marginBottom: 6 }}>
            Tên chủ tài khoản
          </Text>
          <Text
            style={{
              color: '#fff',
              fontSize: 18,
              fontWeight: 500,
              textTransform: 'uppercase',
            }}
          >
            ngoviethung
          </Text>
        </div>

        <div style={{ display: 'flex', flexDirection: 'column' }}>
          <Text style={{ color: '#fff', fontSize: 12, marginBottom: 6 }}>
            NH TMCP NGOAI THUONG - NHTMCP Ngoai Thuong VN - Hoi so chinh
          </Text>
          <Text strong style={{ color: '#fff', fontSize: 18, fontWeight: 500 }}>
            0491000061830
          </Text>
        </div>

        <Dropdown
          overlay={menu}
          style={{ backgroundColor: 'red', borderRadius: '50%', padding: 0 }}
        >
          <Button
            icon={<EditFilled style={{ color: '#111' }} />}
            type='primary'
            shape='circle'
            style={{ position: 'absolute', top: 12, right: 12 }}
            className='btn--default'
          />
        </Dropdown>
      </Card>

      <List style={{ width: '100%' }}>
        <List.Item style={{ padding: '6px 0', border: 'none' }}>
          <List.Item.Meta
            avatar={
              <Avatar
                src={CashWithdrawalSupportLogo}
                alt='cash-withdrawal-support'
              />
            }
            title={<Text style={{ fontSize: 13 }}>Hỗ trợ rút tiền</Text>}
            description={
              <Text style={{ fontSize: 13 }}>
                Rút trực tiếp từ tài khoản COD về tài khoản ngân hàng
              </Text>
            }
          />
        </List.Item>
        <List.Item style={{ padding: '6px 0', border: 'none' }}>
          <List.Item.Meta
            avatar={<Avatar src={FreeTransactionLogo} alt='free-transaction' />}
            title={<Text style={{ fontSize: 13 }}>Miễn phí giao dịch</Text>}
            description={
              <Text style={{ fontSize: 13 }}>Hỗ trợ rút tiền miễn phí</Text>
            }
          />
        </List.Item>
        <List.Item style={{ padding: '6px 0', border: 'none' }}>
          <List.Item.Meta
            avatar={<Avatar src={GiftLogo} alt='gift' />}
            title={<Text style={{ fontSize: 13 }}>Nhận nhiều ưu đãi</Text>}
            description={
              <Text style={{ fontSize: 13 }}>
                Các chương trình ưu đãi hấp dẫn khi thanh toán không dùng tiền
                mặt
              </Text>
            }
          />
        </List.Item>
      </List>

      <div
        style={{
          display: 'flex',
          alignItems: 'flex-start',
          padding: 8,
          backgroundColor: 'rgb(234 231 231)',
          borderRadius: 4,
          marginBottom: 24,
        }}
      >
        <InfoCircleFilled />
        <Text style={{ fontSize: 12, lineHeight: '12px', marginLeft: 4 }}>
          Sử dụng tài khoản ngân hàng để rút tiền COD của dịch vụ Giao Hàng Liên
          Tỉnh
        </Text>
      </div>

      <Space direction='vertical' style={{ width: '100%' }}>
        <Button type='primary' className='btn--warning' onClick={cancel} block>
          Đóng
        </Button>
      </Space>
    </Fragment>
  );
}

export default UserBankInfo;
