import React, { Fragment } from 'react';
import { Form, Select, Typography, Input, Button } from 'antd';
import { openModal } from 'features/modal/modal.slice';
import { useDispatch } from 'react-redux';

const { Text } = Typography;
const { Option } = Select;

AddBankForm.propTypes = {};

function AddBankForm(props) {
  const dispatch = useDispatch();

  const handleConfirmAccount = () =>
    dispatch(openModal({ modalType: 'ConfirmAccountModal' }));

  return (
    <Fragment>
      <Form>
        <Form.Item>
          <Text strong style={{ fontSize: 16, fontWeight: 500, color: '#111' }}>
            Thêm tài khoản
          </Text>
        </Form.Item>
        <Form.Item className='custom-input'>
          <Select placeholder='Chọn tên ngân hàng'>
            <Option key='1'>1</Option>
            <Option key='2'>2</Option>
          </Select>
        </Form.Item>
        <Form.Item className='custom-input'>
          <Select placeholder='Chọn tên chi nhánh'>
            <Option key='1'>1</Option>
            <Option key='2'>2</Option>
          </Select>
        </Form.Item>
        <Form.Item className='custom-input'>
          <Input placeholder='Số tài khoản' />
        </Form.Item>
        <Form.Item className='custom-input'>
          <Input placeholder='Tên chủ tài khoản' />
        </Form.Item>
        <Form.Item>
          <Button
            type='primary'
            block
            className='btn--default'
            onClick={handleConfirmAccount}
          >
            Thêm tài khoản
          </Button>
        </Form.Item>
      </Form>
    </Fragment>
  );
}

export default AddBankForm;
