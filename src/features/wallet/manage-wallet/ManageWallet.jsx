import React from 'react';
// import PropTypes from 'prop-types';
import { Spin, Row, Col } from 'antd';
import LayoutPage from 'app/layout/LayoutPage';
import Header from 'app/layout/Header';
import WalletAction from './WalletAction';
import History from './History';

ManageWallet.propTypes = {};

const histories = [
  {
    key: '1',
    id: '5502',
    order_id: 'HF005507',
    fee: 34000,
    content: 'Nạp tiền tài khoản',
    createdAt: '2020-07-21T03:24:24.290Z',
  },
];

function ManageWallet(props) {
  return (
    <Spin spinning={false}>
      <LayoutPage>
        <Header title='Quản lý ví' />
        <Row style={{ height: '100%', marginTop: 2 }}>
          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
            <WalletAction />
          </Col>
          <Col xs={24} sm={24} md={16} lg={16} xl={16}>
            <History histories={histories} />
          </Col>
        </Row>
      </LayoutPage>
    </Spin>
  );
}

export default ManageWallet;
