import React, { useState } from 'react';
import PropTypes from 'prop-types';
import './manage-wallet.style.css';
import { Tag, Typography, Button, Collapse, Form, Radio, Input } from 'antd';
// import { space } from 'app/utils/config';
import mainAccountImage from 'assets/images/wallet/main-account.png';
import codAccountImage from 'assets/images/wallet/cod-account.png';
import {
  PlusCircleFilled,
  InfoCircleFilled,
  ArrowRightOutlined,
} from '@ant-design/icons';
import { ForwardIcon } from 'app/layout/commons/Icons';
import Price from 'app/layout/commons/Price';
import VnPayLogo from 'assets/images/logo/logo-vnpay.png';
import MomoLogo from 'assets/images/logo/logo-momo.png';
import ZaloPayLogo from 'assets/images/logo/logo-zalopay.png';
import { useDispatch } from 'react-redux';
import { openModal } from 'features/modal/modal.slice';

WalletCard.propTypes = {
  isMainAccount: PropTypes.bool.isRequired,
};

WalletCard.defaultProps = {
  isMainAccount: true,
};

const { Text } = Typography;
const { Panel } = Collapse;

// const baseSpace = space.form.middle;

function WalletCard({ isMainAccount }) {
  const dispatch = useDispatch();
  const [price, setPrice] = useState(100000);
  const [method, setMethod] = useState('VNPay');

  const handleChangeVariant = e => {
    setMethod(e.target.value);
  };

  const handleInputPriceChange = e => setPrice(parseInt(e.target.value));

  const handleSubmit = e => {
    e.preventDefault();

    dispatch(
      openModal({
        modalType: 'QRModal',
        modalProps: {
          price,
          method,
        },
      })
    );
  };

  return (
    <div className='wallet-card'>
      <div className='wallet-card__cover'>
        <img
          alt='cover'
          src={isMainAccount ? mainAccountImage : codAccountImage}
          className='wallet-card__cover-image'
        />
        <div className='wallet-card__cover-container'>
          <div className='wallet-card__cover-container-main-account'>
            <Text style={{ color: '#fff' }}>
              {isMainAccount ? 'Tài khoản chính' : 'Tài khoản COD'}
            </Text>
            <Text
              style={{
                color: isMainAccount ? '#fff' : '#d69006',
                fontSize: 18,
                fontWeight: 500,
              }}
            >
              {isMainAccount ? '250,000 đ' : '0đ'}
            </Text>
          </div>
          <div className='wallet-card__cover-container-free-account'>
            {isMainAccount ? (
              <Text style={{ color: '#fff' }}>Tài khoản khuyến mãi 0đ</Text>
            ) : (
              <Button
                icon={<InfoCircleFilled />}
                shape='round'
                size='small'
                style={{ fontSize: 11 }}
              >
                Hướng dẫn sử dụng
              </Button>
            )}
          </div>
        </div>
      </div>

      <Collapse expandIconPosition='right' className='wallet-card__container'>
        <div className='wallet-card__cover-top-container' />

        <Panel
          header={
            <Tag
              color='success'
              style={{
                backgroundColor: 'transparent',
                border: 'none',
                display: 'flex',
                alignItems: 'center',
              }}
              icon={
                isMainAccount ? (
                  <PlusCircleFilled style={{ fontSize: 16 }} />
                ) : (
                  <ForwardIcon style={{ fontSize: 16 }} />
                )
              }
            >
              <Text style={{ fontWeight: 500, color: '#111' }}>
                {isMainAccount ? 'Nạp tiền' : 'Rút tiền'}
              </Text>
            </Tag>
          }
          key='1'
        >
          <Form layout='vertical'>
            <Form.Item
              label={<Text strong>1. Chọn số tiền (đ)</Text>}
              className='custom-input'
            >
              <Radio.Group
                onChange={e => setPrice(e.target.value)}
                value={price}
                style={{ marginBottom: 12 }}
              >
                <Radio value={100000}>100.000</Radio>
                <Radio value={200000}>200.000</Radio>
                <Radio value={500000}>500.000</Radio>
              </Radio.Group>
              <Input
                placeholder='Nhập số tiền tối thiểu 10.000đ'
                value={price}
                nam='price'
                onChange={handleInputPriceChange}
              />
            </Form.Item>

            <Form.Item label={<Text strong>2. Chọn hình thức nạp tiền</Text>}>
              <div className='radio-custom'>
                <Input
                  type='radio'
                  name='method'
                  value='VNPay'
                  onChange={handleChangeVariant}
                  id='payment-method--vnpay'
                />
                <label htmlFor='payment-method--vnpay' className='radio-label'>
                  <div className='radio-label__header'>
                    <Text style={{ color: '#111' }}>VNPay</Text>
                    <img
                      src={VnPayLogo}
                      alt='vn-pay'
                      style={{ height: '100%' }}
                    />
                  </div>
                  <Text style={{ fontSize: 11 }}>
                    Sử dụng QR code trong ứng dụng ngân hàng để thanh toán
                  </Text>
                </label>
              </div>

              <div className='radio-custom'>
                <Input
                  type='radio'
                  name='method'
                  value='Momo'
                  onChange={handleChangeVariant}
                  id='payment-method--momo'
                />
                <label htmlFor='payment-method--momo' className='radio-label'>
                  <div className='radio-label__header'>
                    <Text style={{ color: '#111' }}>Momo</Text>
                    <img
                      src={MomoLogo}
                      alt='vn-pay'
                      style={{ height: '100%' }}
                    />
                  </div>
                  <Text style={{ fontSize: 11 }}>
                    Thanh toán bằng ví điện tử Momo
                  </Text>
                </label>
              </div>

              <div className='radio-custom'>
                <Input
                  type='radio'
                  name='method'
                  value='ZaloPay'
                  onChange={handleChangeVariant}
                  id='payment-method--zalopay'
                />
                <label
                  htmlFor='payment-method--zalopay'
                  className='radio-label'
                >
                  <div className='radio-label__header'>
                    <Text style={{ color: '#111' }}>ZaloPay</Text>
                    <img
                      src={ZaloPayLogo}
                      alt='vn-pay'
                      style={{ height: '100%' }}
                    />
                  </div>
                  <Text style={{ fontSize: 11 }}>
                    Thanh toán bằng ví điện tử ZaloPay
                  </Text>
                </label>
              </div>
            </Form.Item>
            <Form.Item style={{ marginBottom: 0 }}>
              <Button
                type='primary'
                block
                size='large'
                style={{
                  fontSize: 12,
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                }}
                onClick={handleSubmit}
              >
                <Text style={{ color: '#fff' }}>Nạp tiền qua {method}</Text>
                <Text style={{ color: '#fff' }}>
                  <Price priceString={price} /> <ArrowRightOutlined />
                </Text>
              </Button>
            </Form.Item>
          </Form>
        </Panel>
      </Collapse>
    </div>
  );
}

export default WalletCard;
