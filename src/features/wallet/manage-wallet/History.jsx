import React from 'react';
import PropTypes from 'prop-types';
import { Card, Table, Tag, Typography } from 'antd';
import { PieChartFilled } from '@ant-design/icons';
import Price from 'app/layout/commons/Price';
import { formatDate } from 'app/utils/helper';

History.propTypes = {
  histories: PropTypes.array.isRequired,
};

const columns = [
  {
    title: '#',
    dataIndex: 'key',
    key: 'id',
  },
  {
    title: 'ID',
    dataIndex: 'id',
    key: 'id',
  },
  {
    title: 'Mã đơn hàng',
    dataIndex: 'order_id',
    key: 'order_id',
  },
  {
    title: 'Số tiền',
    dataIndex: 'fee',
    key: 'fee',
  },
  {
    title: 'Nội dung',
    dataIndex: 'content',
    key: 'content',
  },
  {
    title: 'Ngày tạo',
    dataIndex: 'createdAt',
    key: 'createdAt',
  },
];

const { Text } = Typography;

function History({ histories }) {
  const renderHistories = histories.map((history, index) => ({
    key: <Text>{index + 1}</Text>,
    id: <Text>{history.id}</Text>,
    order_id: <Text># {history.order_id}</Text>,
    fee: <Price priceString={history.fee} />,
    content: history.content,
    createdAt: <Text>{formatDate(history.createdAt)}</Text>,
  }));
  return (
    <Card bordered bodyStyle={{ padding: 0 }} style={{ height: '100%' }}>
      <Table
        pagination={{ hideOnSinglePage: true }}
        columns={columns}
        dataSource={renderHistories}
        rowClassName=''
        title={() => (
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
            }}
          >
            <Text strong style={{ fontSize: 16 }}>
              Lịch sử giao dịch
            </Text>
            <Tag icon={<PieChartFilled />}>Thống kê COD</Tag>
          </div>
        )}
      />
    </Card>
  );
}

export default History;
