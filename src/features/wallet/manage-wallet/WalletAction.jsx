import React from 'react';
// import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Tag, Button } from 'antd';
import { space } from 'app/utils/config';
import { RightOutlined, CreditCardFilled } from '@ant-design/icons';
import WalletCard from './WalletCard';
import { openModal } from 'features/modal/modal.slice';

WalletAction.propTypes = {};

const baseSpace = space.form.middle;

function WalletAction(props) {
  const dispatch = useDispatch();

  const handleAddBank = () => dispatch(openModal({ modalType: 'AddBankInfo' }));

  return (
    <div
      style={{
        padding: baseSpace,
        height: '100%',
      }}
    >
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          padding: baseSpace,
          backgroundColor: '#fff',
          borderRadius: 4,
        }}
      >
        <Tag
          icon={<CreditCardFilled />}
          style={{ backgroundColor: 'transparent', border: 'none' }}
        >
          Ngân hàng
        </Tag>
        <Button type='link' onClick={handleAddBank}>
          Thêm ngân hàng ngay {<RightOutlined />}
        </Button>
      </div>

      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          height: '100%',
        }}
      >
        <WalletCard />

        <WalletCard isMainAccount={false} />
      </div>
    </div>
  );
}

export default WalletAction;
