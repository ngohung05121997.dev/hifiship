import React from 'react';
import { Table, Select, Button, Typography, Tag, Space } from 'antd';
import LayoutPage from 'app/layout/LayoutPage';
import Header from 'app/layout/Header';
import { PlusCircleFilled, EditFilled } from '@ant-design/icons';
import { DiscountIcon } from 'app/layout/commons/Icons';
import Price from 'app/layout/commons/Price';
import { formatDate } from 'app/utils/helper';
import { pages } from 'app/utils/config';
import { Link } from 'react-router-dom';

const { Text } = Typography;
const { Option } = Select;

const dataSource = [1, 2];

const columns = [
  {
    title: (
      <Text style={{ fontSize: 12 }}>
        Mã giảm giá|Tên chương trình giảm giá
      </Text>
    ),
    dataIndex: 'discount_code',
    key: 'discount_code',
  },
  {
    title: <Text style={{ fontSize: 12 }}>Loại mã</Text>,
    dataIndex: 'type_code',
    key: 'type_code',
  },
  {
    title: <Text style={{ fontSize: 12 }}>Mức giảm</Text>,
    dataIndex: 'discount_amount',
    key: 'discount_amount',
  },
  {
    title: <Text style={{ fontSize: 12 }}>Số lượng</Text>,
    dataIndex: 'total_discount',
    key: 'total_discount',
  },
  {
    title: <Text style={{ fontSize: 12 }}>Đã dùng</Text>,
    dataIndex: 'number_discount_used',
    key: 'number_discount_used',
  },
  {
    title: <Text style={{ fontSize: 12 }}>Tình trạng</Text>,
    dataIndex: 'status',
    key: 'status',
  },
  {
    title: <Text style={{ fontSize: 12 }}>Thời gian</Text>,
    dataIndex: 'time',
    key: 'time',
  },
  {
    title: <Text style={{ fontSize: 12 }}>Xem chi tiết</Text>,
    dataIndex: 'view_discount',
    key: 'view_discount',
  },
  {
    title: <Text style={{ fontSize: 12 }}>Thao tác</Text>,
    dataIndex: 'actions',
    key: 'actions',
  },
];

DiscountCodeList.propTypes = {};

function DiscountCodeList(props) {
  const renderDataSource = dataSource.map((item, index) => ({
    key: index,
    discount_code: (
      <div style={{ display: 'flex', alignItems: 'center' }}>
        <DiscountIcon style={{ fontSize: 32 }} />
        <div
          style={{ display: 'flex', flexDirection: 'column', marginLeft: 4 }}
        >
          <Text style={{ fontSize: 11 }}>Hifi001</Text>
          <Text style={{ fontSize: 11 }}>Ưu đãi giảm giá tháng 8</Text>
        </div>
      </div>
    ),
    type_code: <Text style={{ fontSize: 11 }}>Siêu tốc</Text>,
    discount_amount: (
      <Text style={{ fontSize: 11 }}>
        <Price priceString={5000} />
      </Text>
    ),
    total_discount: <Text style={{ fontSize: 11 }}>1000</Text>,
    number_discount_used: <Text style={{ fontSize: 11 }}>50</Text>,
    status: (
      <Tag color='#1DA57A' style={{ fontSize: 11 }}>
        Đang chạy
      </Tag>
    ),
    time: (
      <Text style={{ fontSize: 11 }}>
        {formatDate('2020-07-24T03:40:49.299Z')}
      </Text>
    ),
    view_discount: (
      <Tag style={{ fontSize: 11 }}>
        <Link
          to={`${pages.DISCOUNT_CODE_DETAIL.path.replace(':discountId', item)}`}
        >
          Xem
        </Link>
      </Tag>
    ),
    actions: (
      <Space>
        <Button
          size='small'
          icon={<EditFilled style={{ fontSize: 11 }} />}
          type='primary'
          className='btn--success'
        />
      </Space>
    ),
  }));

  return (
    <LayoutPage>
      <Header title='Mã giảm giá' exportRequire={false} />

      <div
        style={{
          margin: 12,
          height: '100%',
          backgroundColor: '#fff',
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'flex-start',
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'flex-start',
            alignItems: 'center',
            padding: 12,
          }}
        >
          <Select
            style={{ width: 200, borderRadius: 24, marginRight: 12 }}
            className='custom-select'
            defaultValue='Tất cả'
          >
            <Option key='Tất cả'>Tất cả</Option>
            <Option key='Chưa chạy'>Chưa chạy</Option>
            <Option key='Đang chạy'>Đang chạy</Option>
            <Option key='Kết thúc'>Kết thúc</Option>
          </Select>
          <Link to={`${pages.CREATE_DISCOUNT.path}`}>
            <Button
              type='primary'
              className='btn--warning'
              icon={<PlusCircleFilled />}
              shape='round'
            >
              Tạo mã mới
            </Button>
          </Link>
        </div>
        <Table
          size='small'
          columns={columns}
          dataSource={renderDataSource}
          pagination={false}
          bordered
          style={{ width: '100%' }}
        />
      </div>
    </LayoutPage>
  );
}

export default DiscountCodeList;
