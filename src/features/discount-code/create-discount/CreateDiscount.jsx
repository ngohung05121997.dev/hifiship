import React from 'react';
import './create-discount.style.css';
import { useHistory } from 'react-router-dom';
import {
  Form,
  Input,
  Button,
  Typography,
  InputNumber,
  Row,
  Col,
  Radio,
  Select,
  TimePicker,
  DatePicker,
  Space,
  Card,
} from 'antd';
import moment from 'moment';
import LayoutPage from 'app/layout/LayoutPage';
import Header from 'app/layout/Header';
import {
  CalendarFilled,
  ClockCircleFilled,
  InfoCircleFilled,
  ThunderboltFilled,
  CrownFilled,
  GiftFilled,
} from '@ant-design/icons';
import GuideImage from 'assets/images/bank/guide.png';

CreateDiscount.propTypes = {};

const { Text } = Typography;
const { Option } = Select;

const inputStyle = {
  style: {
    borderRadius: 24,
  },
};

function CreateDiscount(props) {
  const history = useHistory();

  const goBack = () => history.goBack();

  const onTimeChange = (time, timeString) => {
    console.log(time, timeString);
  };

  const onDateChange = () => {};

  return (
    <LayoutPage>
      <Header
        title='Tạo mã giảm giá'
        goBackRequire={true}
        goBack={goBack}
        exportRequire={false}
      />

      <div
        style={{ margin: 12, height: '100%', padding: '32px 98px' }}
        className='bg-white'
      >
        <Row>
          <Col xs={24} sm={24} md={24} lg={16} xl={16}>
            <Form
              layout='horizontal'
              labelCol={{ span: 8 }}
              wrapperCol={{ span: 16 }}
              labelAlign='left'
              className='create-discount-form'
            >
              <Form.Item>
                <Text>Thông tin mã giảm giá</Text>
              </Form.Item>
              <Form.Item label='Tên chương trình giảm giá'>
                <Input {...inputStyle} />
              </Form.Item>
              <Form.Item
                label='Mã giảm giá'
                help={
                  <Text type='danger' style={{ fontSize: 11 }}>
                    Vui lòng chỉ nhập các ký tự chữ cái (A-Z), số (0-9)
                  </Text>
                }
              >
                <Input {...inputStyle} />
              </Form.Item>
              <Form.Item label='Loại voucher'>
                <Radio.Group name='voucher_type'>
                  <Radio>Khuyến mãi</Radio>
                  <Radio>Hoàn ví</Radio>
                </Radio.Group>
              </Form.Item>
              <Form.Item label='Loại giảm giá | Mức giảm'>
                <Input
                  {...inputStyle}
                  addonBefore={
                    <Select defaultValue='Theo số tiền'>
                      <Option value='Theo số tiền'>Theo số tiền</Option>
                      <Option value='1'>1</Option>
                    </Select>
                  }
                />
              </Form.Item>
              <Form.Item label='Giá trị đơn hàng tối thiểu'>
                <InputNumber
                  style={{ borderRadius: 24, width: '100%' }}
                  min='0'
                  placeholder='đ'
                />
              </Form.Item>
              <Form.Item label='Thời gian bắt đầu'>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                >
                  <TimePicker
                    onChange={onTimeChange}
                    defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}
                    {...inputStyle}
                    suffixIcon={<ClockCircleFilled />}
                    placeholder='VD: 17:00'
                  />
                  <DatePicker
                    onChange={onDateChange}
                    style={{ flex: 1, marginLeft: 12, borderRadius: 24 }}
                    placeholder='VD: 17/07/2020'
                    suffixIcon={<CalendarFilled />}
                  />
                </div>
              </Form.Item>
              <Form.Item label='Thời gian kết thúc'>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                  }}
                >
                  <TimePicker
                    onChange={onTimeChange}
                    defaultOpenValue={moment('00:00:00', 'HH:mm:ss')}
                    {...inputStyle}
                    suffixIcon={<ClockCircleFilled />}
                    placeholder='VD: 17:00'
                  />
                  <DatePicker
                    onChange={onDateChange}
                    style={{ flex: 1, marginLeft: 12, borderRadius: 24 }}
                    placeholder='VD: 17/07/2020'
                    suffixIcon={<CalendarFilled />}
                  />
                </div>
              </Form.Item>
              <Form.Item label='Số lượng Mã giảm giá'>
                <InputNumber
                  style={{ borderRadius: 24, width: '100%' }}
                  min='0'
                  placeholder='Tổng số mã giảm giá'
                />
              </Form.Item>
              <Form.Item label='Đối tượng hưởng mã giảm giá'>
                <div
                  style={{
                    display: 'flex',
                    flexDirection: 'column',
                    justifyContent: 'flex-start',
                  }}
                >
                  <Radio.Group name='customer_type' style={{ marginBottom: 6 }}>
                    <Radio>Tất cả khách hàng</Radio>
                    <Radio>Theo danh sách</Radio>
                  </Radio.Group>
                  <Input
                    placeholder='Nhập tên, ID khách hàng được hưởng'
                    {...inputStyle}
                  />
                </div>
              </Form.Item>
              <Form.Item label='Dịch vụ khuyến mãi'>
                <Row gutter={16}>
                  <Col span={8}>
                    <Card
                      bordered
                      hoverable
                      style={{ position: 'relative' }}
                      bodyStyle={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingTop: 4,
                        paddingBottom: 4,
                      }}
                    >
                      <InfoCircleFilled
                        style={{
                          position: 'absolute',
                          top: 2,
                          right: 2,
                          fontSize: 11,
                        }}
                      />
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'column',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <ThunderboltFilled />
                        <Text style={{ fontSize: 11 }}>Siêu tốc</Text>
                      </div>
                    </Card>
                  </Col>
                  <Col span={8}>
                    <Card
                      bordered
                      hoverable
                      style={{ position: 'relative' }}
                      bodyStyle={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingTop: 4,
                        paddingBottom: 4,
                      }}
                    >
                      <InfoCircleFilled
                        style={{
                          position: 'absolute',
                          top: 2,
                          right: 2,
                          fontSize: 11,
                        }}
                      />
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'column',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <CrownFilled />
                        <Text style={{ fontSize: 11 }}>Siêu rẻ</Text>
                      </div>
                    </Card>
                  </Col>
                  <Col span={8}>
                    <Card
                      bordered
                      hoverable
                      style={{ position: 'relative' }}
                      bodyStyle={{
                        display: 'flex',
                        justifyContent: 'center',
                        alignItems: 'center',
                        paddingTop: 4,
                        paddingBottom: 4,
                      }}
                    >
                      <InfoCircleFilled
                        style={{
                          position: 'absolute',
                          top: 2,
                          right: 2,
                          fontSize: 11,
                        }}
                      />
                      <div
                        style={{
                          display: 'flex',
                          flexDirection: 'column',
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}
                      >
                        <GiftFilled />
                        <Text style={{ fontSize: 11 }}>Đồng giá</Text>
                      </div>
                    </Card>
                  </Col>
                </Row>
              </Form.Item>

              <Form.Item wrapperCol={{ span: 24 }}>
                <Space size={12}>
                  <Button
                    htmlType='submit'
                    type='primary'
                    block
                    className='btn--warning'
                    style={{ width: 150 }}
                  >
                    Lưu
                  </Button>
                  <Button block className='btn--default' style={{ width: 150 }}>
                    Hủy
                  </Button>
                </Space>
              </Form.Item>
            </Form>
          </Col>
          <Col xs={24} sm={24} md={24} lg={8} xl={8}>
            <div
              style={{
                padding: 12,
                display: 'flex',
                flexDirection: 'column',
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}
            >
              <img alt='guide' src={GuideImage} style={{ width: '80%' }} />
            </div>
          </Col>
        </Row>
      </div>
    </LayoutPage>
  );
}

export default CreateDiscount;
