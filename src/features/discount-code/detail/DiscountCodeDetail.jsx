import React from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { Typography, Table, Tag, Space } from 'antd';
import LayoutPage from 'app/layout/LayoutPage';
import Header from 'app/layout/Header';
import { DiscountIcon } from 'app/layout/commons/Icons';
import Price from 'app/layout/commons/Price';
import { formatDate } from 'app/utils/helper';

const { Text } = Typography;

DiscountCodeDetail.propTypes = {
  discount: PropTypes.object.isRequired,
};

DiscountCodeDetail.defaultProps = {
  discount: {},
};

const dataSource = [
  {
    key: '1',
    name: 'Mike',
    age: 32,
    address: '10 Downing Street',
  },
  {
    key: '2',
    name: 'John',
    age: 42,
    address: '10 Downing Street',
  },
];

const columns = [
  {
    title: <Text style={{ fontSize: 11 }}>Mã đơn hàng</Text>,
    dataIndex: 'order_id',
    key: 'order_id',
  },
  {
    title: <Text style={{ fontSize: 11 }}>Loại đơn</Text>,
    dataIndex: 'order_type',
    key: 'order_type',
  },
  {
    title: <Text style={{ fontSize: 11 }}>Mức giảm</Text>,
    dataIndex: 'discount_amount',
    key: 'discount_amount',
  },
  {
    title: <Text style={{ fontSize: 11 }}>Tổng số tiền</Text>,
    dataIndex: 'total_amount',
    key: 'total_amount',
  },
  {
    title: <Text style={{ fontSize: 11 }}>Ngày dùng</Text>,
    dataIndex: 'date',
    key: 'date',
  },
  {
    title: <Text style={{ fontSize: 11 }}>Trạng thái đơn hàng</Text>,
    dataIndex: 'order_status',
    key: 'order_status',
  },
];

function DiscountCodeDetail({ discount }) {
  const history = useHistory();

  const goBack = () => history.goBack();

  const renderDataSource = dataSource.map((item, index) => ({
    key: index,
    order_id: <Text style={{ fontSize: 11 }}>HF0000152</Text>,
    order_type: <Text style={{ fontSize: 11 }}>Siêu tốc</Text>,
    discount_amount: (
      <Text style={{ fontSize: 11 }}>
        <Price priceString={5000} />
      </Text>
    ),
    total_amount: (
      <Text style={{ fontSize: 11 }}>
        <Price priceString={75000} />
      </Text>
    ),
    date: (
      <Text style={{ fontSize: 11 }}>
        {formatDate('2020-07-24T05:49:28.328Z')}
      </Text>
    ),
    order_status: <Text style={{ fontSize: 11 }}>Đã giao</Text>,
  }));

  return (
    <LayoutPage>
      <Header
        title='Chi tiết mã giảm giá'
        goBackRequire={true}
        goBack={goBack}
        exportRequire={false}
      />
      <div
        className='bg-search'
        style={{
          padding: 12,
          height: '100%',
          display: 'flex',
          flexDirection: 'column',
        }}
      >
        <div
          style={{
            marginBottom: 12,
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
          }}
          className='bg-white'
        >
          <div
            style={{
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              padding: '6px 12px',
              backgroundColor: '#d8d8d8',
            }}
          >
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <DiscountIcon style={{ fontSize: 32 }} />
              <div
                style={{
                  display: 'flex',
                  flexDirection: 'column',
                  marginLeft: 4,
                }}
              >
                <Text style={{ fontSize: 11 }}>Hifi001</Text>
                <Text style={{ fontSize: 11 }}>Ưu đãi giảm giá tháng 8</Text>
              </div>
            </div>

            <Tag color='#1DA57A' style={{ fontSize: 11 }}>
              Đang chạy
            </Tag>
          </div>
          <div style={{ padding: '6px 12px', display: 'flex' }}>
            <Space size={48}>
              <div style={{ display: 'flex', flexDirection: 'column' }}>
                <Text style={{ fontSize: 11 }}>Mức giảm: 5.000 đ</Text>
                <Text style={{ fontSize: 11 }}>
                  Giá trị đơn hàng tối thiểu: 50.000 đ
                </Text>
              </div>
              <div style={{ display: 'flex', flexDirection: 'column' }}>
                <Text style={{ fontSize: 11 }}>
                  Thời gian áp dụng: time from - time to
                </Text>
                <Text style={{ fontSize: 11 }}>Số lượng: 1000</Text>
              </div>
              <div style={{ display: 'flex', flexDirection: 'column' }}>
                <Text style={{ fontSize: 11 }}>Loại mã: Siêu tốc</Text>
                <Text style={{ fontSize: 11 }}>Đã dùng: 50</Text>
              </div>
            </Space>
          </div>
        </div>

        <Table
          bordered
          pagination={false}
          title={() => (
            <Text strong style={{ color: '#111' }}>
              Đơn hàng đã áp dụng mã giảm giá (50)
            </Text>
          )}
          columns={columns}
          dataSource={renderDataSource}
          size='small'
        />
      </div>
    </LayoutPage>
  );
}

export default DiscountCodeDetail;
