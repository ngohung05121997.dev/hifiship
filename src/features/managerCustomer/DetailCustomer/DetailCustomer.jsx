import React from "react";
import LayoutPage from "app/layout/LayoutPage";
import Header from "app/layout/Header";

function DetailCustomer() {
  return (
    <LayoutPage>
      <Header
        onBack={() => window.history.back()}
        title="Chi tiết khách hàng"
      />
    </LayoutPage>
  );
}

export default DetailCustomer;
