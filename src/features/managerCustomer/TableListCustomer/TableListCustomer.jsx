import React from "react";
import { Table } from "antd";
import { EyeFilled, EditFilled } from "@ant-design/icons";
import { Link } from "react-router-dom";
import { pages } from "app/utils/config";

const { Column } = Table;
function TableListCustomer() {
  const dataSource = [
    {
      key: "1",
      stt: "1",
      id: "HF0000112",
      name: "Bui Than",
      phone: "0987656949",
      email: "buithan92@gmail.com",
      area: "Hà Đông",
      createdDate: "29/07/2020 15:55",
      note: "",
      customCare: "Bui Than",
      business: "Bui Than",
      edit: "",
    },
    {
      key: "2",
      stt: "2",
      id: "HF0000112",
      name: "Bui Than",
      phone: "0987656949",
      email: "buithan92@gmail.com",
      area: "Hà Đông",
      createdDate: "29/07/2020 15:55",
      note: "",
      customCare: "Bui Than",
      business: "Bui Than",
      edit: "",
    },
    {
      key: "3",
      stt: "3",
      id: "HF0000112",
      name: "Bui Than",
      phone: "0987656949",
      email: "buithan92@gmail.com",
      area: "Hà Đông",
      createdDate: "29/07/2020 15:55",
      note: "",
      customCare: "Bui Than",
      business: "Bui Than",
      edit: "",
    },
    {
      key: "4",
      stt: "4",
      id: "HF0000112",
      name: "Bui Than",
      phone: "0987656949",
      email: "buithan92@gmail.com",
      area: "Hà Đông",
      createdDate: "29/07/2020 15:55",
      note: "",
      customCare: "Bui Than",
      business: "Bui Than",
      edit: "",
    },
  ];

  return (
    <Table dataSource={dataSource} bordered className="tb-custom">
      <Column title="#" dataIndex="stt" key="Số thứ tự"></Column>
      <Column
        title="ID"
        dataIndex="id"
        key="Mã đơn hàng"
        render={(id) => (
          <Link to={pages.DETAIL_CUSTOMER.path.replace(":customId", id)}>
            {id}
          </Link>
        )}
      ></Column>
      <Column title="Tên KH" dataIndex="name" key="name"></Column>
      <Column title="Số điện thoại" dataIndex="phone" key="phone"></Column>
      <Column title="Email" dataIndex="email" key="email"></Column>
      <Column title="Khu vực" dataIndex="area" key="area"></Column>
      <Column
        title="Ngày tạo"
        dataIndex="createdDate"
        key="createdDate"
      ></Column>
      <Column
        title="Ghi chú"
        dataIndex="note"
        key="note"
        render={() => (
          <div className="icon-eye">
            <EyeFilled />
          </div>
        )}
      ></Column>
      <Column
        title="Chăm sóc KH"
        dataIndex="customCare"
        key="customCare"
      ></Column>
      <Column title="Kinh doanh" dataIndex="business" key="business"></Column>
      <Column
        title="Thao tác"
        dataIndex="edit"
        key="edit"
        render={() => (
          <div className="icon-edit">
            <EditFilled />
          </div>
        )}
      ></Column>
    </Table>
  );
}

export default TableListCustomer;
