import React from 'react';
import LayoutPage from 'app/layout/LayoutPage';
import { Tabs, Input, Row, Col, Select } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import TableListCustomer from './TableListCustomer/TableListCustomer';
import Header from 'app/layout/Header';

const { TabPane } = Tabs;
const { Option } = Select;

function ListCustomer() {
  return (
    <LayoutPage>
      <Header title='Danh sách khách hàng' />
      <div className='tab-custom'>
        <Tabs defaultActiveKey='1'>
          <TabPane tab='Tất cả' key='1'>
            <div className='bg-white'>
              <Row className='search-tb'>
                <Col lg='12' md='12' sm='12' className='mr-10'>
                  <Input
                    className='bg-search'
                    placeholder='Tìm kiếm'
                    prefix={<SearchOutlined />}
                  />
                </Col>
                <Col lg='6' md='6' sm='6'>
                  <Select defaultValue='Tuần này'>
                    <Option value='all'>Tất cả</Option>
                    <Option value='today'>Hôm nay</Option>
                    <Option value='week'>Tuần này</Option>
                    <Option value='month'>Tháng này</Option>
                    <Option value='edit'>Tùy chỉnh</Option>
                  </Select>
                </Col>
              </Row>
              <TableListCustomer />
            </div>
          </TabPane>
          <TabPane tab='Đang giao' key='2'></TabPane>
          <TabPane tab='Khóa tạm thời' key='3'></TabPane>
          <TabPane tab='Khóa vĩnh viễn' key='4'></TabPane>
        </Tabs>
      </div>
    </LayoutPage>
  );
}

export default ListCustomer;
