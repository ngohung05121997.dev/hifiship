import React from 'react';
import { Spin, Tabs } from 'antd';
import LayoutPage from 'app/layout/LayoutPage';
import OrderHistoryTab from './OrderHistoryTab';
import Header from 'app/layout/Header';

const { TabPane } = Tabs;

function OrderHistory(props) {
  return (
    <LayoutPage>
      <Spin spinning={false}>
        <Header title='Lịch sử đơn hàng' />
        <div className='tab-custom'>
          <Tabs>
            <TabPane tab='Tất cả' key='Tất cả'>
              <OrderHistoryTab />
            </TabPane>
            <TabPane tab='Đang chờ' key='Đang chờ'>
              <OrderHistoryTab />
            </TabPane>
            <TabPane tab='Đang tìm' key='Đang tìm'>
              <OrderHistoryTab />
            </TabPane>
            <TabPane tab='Đã nhận' key='Đã nhận'>
              <OrderHistoryTab />
            </TabPane>
            <TabPane tab='Đang giao' key='Đang giao'>
              <OrderHistoryTab />
            </TabPane>
            <TabPane tab='Hoàn thành' key='Hoàn thành'>
              <OrderHistoryTab />
            </TabPane>
            <TabPane tab='Đã hủy' key='Đã hủy'>
              <OrderHistoryTab />
            </TabPane>
          </Tabs>
        </div>
      </Spin>
    </LayoutPage>
  );
}

export default OrderHistory;
