import React from 'react';
import './history.style.css';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Card, Table, Layout, Input, Typography, Tag, Space } from 'antd';
import { space } from 'app/utils/config';
import { ClockCircleFilled, SearchOutlined } from '@ant-design/icons';
import { formatDate } from 'app/utils/helper';
import { pages } from 'app/utils/config';
import Price from 'app/layout/commons/Price';
import { MoneyIcon } from 'app/layout/commons/Icons';

OrderHistoryTab.propTypes = {
  type: PropTypes.string.isRequired,
};

OrderHistoryTab.defaultProps = {
  type: 'test',
};

const { Text } = Typography;
const baseSpace = space.form.middle;

const SmallText = ({ children }) => (
  <Text style={{ fontSize: 11 }}>{children}</Text>
);

const TitleText = ({ children }) => (
  <Text strong style={{ fontSize: 12, fontWeight: 500 }}>
    {children}
  </Text>
);

const configData = [
  {
    key: '1',
    order_id: 'HF005502',
    sender: {
      name: 'Nguyễn Văn Dũng',
      phone: '0979027139',
      address: 'Maidzo, số 5 49/64 Huỳnh Thúc Kháng, Láng Hạ, Đống Đa, Hà Nội',
    },
    receiver: {
      name: 'Hưng',
      phone: '0966382597',
      address: 'HH02-D Thanh Hà, Cự Khê, Thanh Oai, Hà Đông, Hà Nội',
    },
    sender_time: '2020-07-20T07:26:54.105Z',
    receiver_time: '2020-07-20T07:26:54.105Z',
    cod: 120000,
    fee: 150000,
    status: 'success',
  },
  {
    key: '2',
    order_id: 'HF005502',
    sender: {
      name: 'Nguyễn Văn Dũng',
      phone: '0979027139',
      address: 'Maidzo, số 5 49/64 Huỳnh Thúc Kháng, Láng Hạ, Đống Đa, Hà Nội',
    },
    receiver: {
      name: 'Hưng',
      phone: '0966382597',
      address: 'HH02-D Thanh Hà, Cự Khê, Thanh Oai, Hà Đông, Hà Nội',
    },
    sender_time: '2020-07-20T07:26:54.105Z',
    receiver_time: '2020-07-20T07:26:54.105Z',
    cod: 120000,
    fee: 150000,
    status: 'success',
  },
];

const columns = [
  {
    title: <TitleText>Mã đơn hàng</TitleText>,
    dataIndex: 'order_id',
    key: 'Mã đơn hàng',
  },
  {
    title: <TitleText>Điểm lấy hàng</TitleText>,
    dataIndex: 'sender',
    key: 'Điểm lấy hàng',
  },
  {
    title: <TitleText>Điểm giao hàng</TitleText>,
    dataIndex: 'receiver',
    key: 'Điểm giao hàng',
  },
  {
    title: <TitleText>Thời gian giao</TitleText>,
    dataIndex: 'sender_time',
    key: 'Thời gian giao',
  },
  {
    title: <TitleText>Thời gian hoàn thành</TitleText>,
    dataIndex: 'receiver_time',
    key: 'Thời gian hoàn thành',
  },
  {
    title: <TitleText>Tổng COD</TitleText>,
    dataIndex: 'cod',
    key: 'Tổng COD',
  },
  {
    title: <TitleText>Tổng phí</TitleText>,
    dataIndex: 'fee',
    key: 'Tổng phí',
  },
  {
    title: <TitleText>Trạng thái</TitleText>,
    dataIndex: 'status',
    key: 'Trạng thái',
  },
];

function OrderHistoryTab({ type }) {
  const dataSource = configData.map(elm => ({
    key: elm.key,
    order_id: (
      <Link to={pages.ORDER_DETAILS.path.replace(':orderId', elm.order_id)}>
        <SmallText strong># {elm.order_id}</SmallText>
      </Link>
    ),
    sender: (
      <Space direction='vertical' size={0}>
        <SmallText>
          {elm.sender.name} - {elm.sender.phone}
        </SmallText>
        <SmallText>{elm.sender.address}</SmallText>
      </Space>
    ),
    receiver: (
      <Space direction='vertical' size={0}>
        <SmallText>
          {elm.receiver.name} - {elm.receiver.phone}
        </SmallText>
        <SmallText>{elm.receiver.address}</SmallText>
      </Space>
    ),
    sender_time: <SmallText>{formatDate(elm.sender_time)}</SmallText>,
    receiver_time: <SmallText>{formatDate(elm.receiver_time)}</SmallText>,
    cod: (
      <SmallText>
        <Tag style={{ border: 'none' }}>
          <Text type='warning' strong style={{ fontWeight: 700 }}>
            <Price priceString={elm.cod} />
          </Text>
        </Tag>
      </SmallText>
    ),
    fee: (
      <SmallText>
        <Tag
          style={{ backgroundColor: '#fff', border: 'none' }}
          icon={<MoneyIcon />}
        >
          <Text type='warning' strong style={{ fontWeight: 700 }}>
            <Price priceString={elm.fee} />
          </Text>
        </Tag>
      </SmallText>
    ),
    status: (
      <Tag style={{ borderRadius: 12, padding: '2px 12px' }} color='success'>
        Thành công
      </Tag>
    ),
  }));

  return (
    <Layout.Content
      style={{
        minHeight: 'calc(100vh - 72px - 62px - 2px)',
      }}
    >
      <Card
        className='custom-card'
        bodyStyle={{ padding: baseSpace }}
        style={{ height: '100%' }}
      >
        <Table
          pagination={{ hideOnSinglePage: true, size: 'small' }}
          columns={columns}
          dataSource={dataSource}
          showHeader
          title={() => (
            <Space size={16}>
              <div className='custom-input'>
                <Input
                  placeholder='Tìm kiếm'
                  style={{ width: 250 }}
                  prefix={<SearchOutlined />}
                />
              </div>
              <Tag
                icon={<ClockCircleFilled />}
                style={{
                  padding: '2px 12px',
                  borderRadius: 16,
                }}
                className='background-gray'
              >
                Thời gian
              </Tag>
            </Space>
          )}
        />
      </Card>
    </Layout.Content>
  );
}

export default OrderHistoryTab;
