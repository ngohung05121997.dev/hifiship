import { createSlice } from '@reduxjs/toolkit';

const orderInitialState = {
  current: null,
  currentEditing: null,
  orders: [],
};

const orderSlice = createSlice({
  name: 'order',
  initialState: orderInitialState,
  reducers: {
    setCurrentOrder: (state, { payload: { order } }) => {
      state.current = order;
    },
    setOrders: (state, { payload: { orders } }) => {
      state.orders = orders;
    },
    setCurrentEditing: (state, { payload: { currentEditing } }) => {
      state.currentEditing = currentEditing;
    },
    clear: state => {
      state.current = null;
      state.currentEditing = null;
      state.orders = [];
    },
  },
});

export const {
  setCurrentEditing,
  setCurrentOrder,
  setOrders,
  clear,
} = orderSlice.actions;

export default orderSlice.reducer;
