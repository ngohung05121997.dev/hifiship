import React from 'react';
import { Typography } from 'antd';
import { space } from 'app/utils/config';

Footer.propTypes = {};

const { Text } = Typography;
const baseSpace = space.form.middle;

function Footer(props) {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        padding: baseSpace,
      }}
    >
      <Text type='danger' style={{ textAlign: 'center', fontSize: 11 }}>
        *Lưu ý: Không được giao cho tài xế mà không phải tài xế trong ảnh.
      </Text>
      <Text type='danger' style={{ textAlign: 'center', fontSize: 11 }}>
        Chúng tôi sẽ không chịu trách nhiệm khi bạn giao đơn hàng cho người lạ
        hoặc thỏa thuận ngoài với ship.
      </Text>
    </div>
  );
}

export default Footer;
