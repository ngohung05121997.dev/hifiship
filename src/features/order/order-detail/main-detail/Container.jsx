import React from 'react';
// import PropTypes from 'prop-types';
import { Layout, Card, Space, Typography, Tag, Avatar, List } from 'antd';
import { space } from 'app/utils/config';
import { DotIconSuccess } from 'app/layout/commons/Icons';
import { EnvironmentFilled, EditFilled } from '@ant-design/icons';
import { VNDIcon } from 'app/layout/commons/Icons';
import Price from 'app/layout/commons/Price';

Container.propTypes = {};

const { Text } = Typography;
const baseSpace = space.form.middle;

function Container(props) {
  return (
    <Layout style={{ padding: baseSpace }}>
      <Card
        bodyStyle={{ padding: 0 }}
        className='custom-card order-detail__container'
        bordered
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'center',
            padding: baseSpace,
            borderBottom: '1px solid #ccc',
            width: '100%',
          }}
        >
          <Space>
            <Text>Lộ trình</Text>
            <Text className='primary-color'>9,1 km</Text>
          </Space>
          <Tag
            style={{ padding: '2px 12px', borderRadius: 12 }}
            className='background-gray'
          >
            Hoàn thành
          </Tag>
        </div>

        <div
          style={{
            padding: baseSpace,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
          }}
        >
          <div
            style={{ width: '100%', display: 'flex', alignItems: 'flex-start' }}
          >
            <div style={{ marginRight: baseSpace, width: 32, height: 32 }}>
              <Avatar alt='avatar' src='' />
            </div>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                width: '100%',
                flex: 1,
              }}
            >
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'flex-start',
                  width: '100%',
                }}
              >
                <Space size={0} direction='vertical'>
                  <Space size={4}>
                    <Text style={{ color: '#111' }}>Nguyễn Dũng</Text>
                    <Text style={{ color: '#111' }}>|</Text>
                    <Text style={{ color: '#111' }}>849156799866</Text>
                  </Space>
                  <Text>28a, Lê Trọng Tấn, Hà Đông, Hà Nội</Text>
                </Space>
                <Text style={{ fontWeight: 700, fontSize: 12 }}>
                  06/07/2020 11:45
                </Text>
              </div>

              <List
                style={{ width: '100%' }}
                dataSource={[1, 2, 3]}
                renderItem={(item, index) => (
                  <List.Item style={{ padding: 0, border: 'none' }}>
                    <List.Item.Meta
                      style={{
                        display: 'flex',
                        alignItems: 'center',
                      }}
                      avatar={
                        <div
                          style={{
                            width: 32,
                            height: 32,
                            display: 'flex',
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}
                        >
                          {index === 0 ? (
                            <Avatar alt='avatar' src='' />
                          ) : (
                            <DotIconSuccess style={{ fontSize: 24 }} />
                          )}
                        </div>
                      }
                      description={
                        <div
                          style={{
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'flex-start',
                            padding: baseSpace / 2,
                            borderTop: index === 0 ? '1px dashed #ccc' : 'none',
                            borderBottom: '1px dashed #ccc',
                          }}
                        >
                          {index === 0 ? (
                            <Text>Tuấn Nguyễn sẽ giao đơn hàng</Text>
                          ) : index === 1 ? (
                            <Text>Đang giao hàng - 06/07/2020 11:51</Text>
                          ) : (
                            <Text>Hoàn thành - 06/07/202 13:20</Text>
                          )}
                        </div>
                      }
                    />
                  </List.Item>
                )}
              />
            </div>
          </div>
          <div
            style={{ width: '100%', display: 'flex', alignItems: 'flex-start' }}
          >
            <div style={{ marginRight: baseSpace, width: 32, height: 32 }}>
              <EnvironmentFilled
                style={{
                  color: 'red',
                  display: 'flex',
                  alignItems: 'flex-start',
                  justifyContent: 'center',
                }}
              />
            </div>
            <div
              style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                width: '100%',
                flex: 1,
              }}
            >
              <div
                style={{
                  display: 'flex',
                  justifyContent: 'space-between',
                  alignItems: 'center',
                  width: '100%',
                }}
              >
                <Space size={0} direction='vertical'>
                  <Space size={4}>
                    <Text style={{ color: '#111' }}>Hung</Text>
                    <Text style={{ color: '#111' }}>|</Text>
                    <Text style={{ color: '#111' }}>0966382597</Text>
                  </Space>

                  <Text>HH02-D Thanh Hà, Cự Khê, Thanh Oai, Hà Đông</Text>
                </Space>
              </div>
            </div>
          </div>
          <div
            style={{
              display: 'flex',
              flexDirection: 'column',
              alignItems: 'flex-start',
              width: '100%',
            }}
          >
            <Text>Ghi chú</Text>
            <div
              style={{
                padding: '2px 4px',
                border: '1px solid #ccc',
                width: '100%',
                borderRadius: 4,
              }}
            >
              <Text>Ship trước 14h cho khách</Text>
            </div>
            <div>
              <Text style={{ fontWeight: 500, color: '#111' }}>
                Tổng thời gian hoàn thành đơn: 1h21'
              </Text>
            </div>
          </div>
        </div>
      </Card>
      <div style={{ marginTop: baseSpace }}>
        <div>
          <Text>Phụ thu (shipper)</Text>
        </div>
        <div
          style={{
            display: 'flex',
            alignItems: 'center',
            marginBottom: baseSpace,
          }}
        >
          <Tag
            icon={<VNDIcon />}
            style={{
              display: 'flex',
              flexDirection: 'row-reverse',
              alignItems: 'center',
              justifyContent: 'space-between',
              padding: 10,
              flex: 1,
              backgroundColor: '#fff',
            }}
          >
            Phụ phí
          </Tag>
          <Tag
            icon={<EditFilled />}
            style={{
              display: 'flex',
              flexDirection: 'row-reverse',
              alignItems: 'center',
              justifyContent: 'space-between',
              padding: 10,
              flex: 1,
              backgroundColor: '#fff',
              marginRight: 0,
            }}
          >
            Ghi chú
          </Tag>
        </div>
        <div>
          <List bordered style={{ backgroundColor: '#fff' }}>
            <List.Item style={{ padding: baseSpace }}>
              <Text>Phí dịch vụ</Text>
              <Text>
                <Price priceString={28000} />
              </Text>
            </List.Item>
            <List.Item style={{ padding: baseSpace }}>
              <Text>Thu hộ COD</Text>
              <Text>
                <Price priceString={0} />
              </Text>
            </List.Item>
            <List.Item style={{ padding: baseSpace }}>
              <Text>Quay lại điểm lấy hàng</Text>
              <Text>
                <Price priceString={0} />
              </Text>
            </List.Item>
            <List.Item style={{ padding: baseSpace }}>
              <Text>Hỗ trợ tài xế</Text>
              <Text>
                <Price priceString={5000} />
              </Text>
            </List.Item>
            <List.Item style={{ padding: baseSpace }}>
              <Text>Tổng giá trị (bao gồm VAT)</Text>
              <Text>
                <Price priceString={28000} />
              </Text>
            </List.Item>
            <List.Item style={{ padding: baseSpace }}>
              <Text strong style={{ color: '#111', fontWeight: 500 }}>
                Tổng cộng
              </Text>
              <Text type='warning' style={{ fontWeight: 500 }}>
                <Price priceString={33000} />
              </Text>
            </List.Item>
          </List>
        </div>
      </div>
    </Layout>
  );
}

export default Container;
