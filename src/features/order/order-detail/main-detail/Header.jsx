import React from 'react';
import PropTypes from 'prop-types';
import { Card, Typography, Space, Button, Tag } from 'antd';
import {
  ArrowLeftOutlined,
  InfoCircleFilled,
  PrinterFilled,
  CrownFilled,
  EllipsisOutlined,
} from '@ant-design/icons';

Header.propTypes = {
  goBack: PropTypes.func.isRequired,
};

const { Text } = Typography;

function Header({ goBack }) {
  return (
    <Card bordered bodyStyle={{ padding: 12 }}>
      <Card.Meta
        title={<Text strong>Đơn hàng #HF005502</Text>}
        style={{ marginBottom: 12 }}
      />
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <Space size={2}>
          <Button
            type='link'
            shape='circle'
            icon={<ArrowLeftOutlined className='primary-color' />}
            onClick={goBack}
          />
          <Tag
            icon={<CrownFilled className='warning-color' />}
            style={{ padding: '2px 12px', borderRadius: 12 }}
          >
            Siêu rẻ <InfoCircleFilled />
          </Tag>
        </Space>
        <Space size={2}>
          <Button
            type='link'
            shape='circle'
            icon={<PrinterFilled />}
            className='background-gray'
            style={{ color: '#111' }}
          />
          <Button
            type='link'
            shape='circle'
            icon={<EllipsisOutlined />}
            className='background-gray'
            style={{ color: '#111' }}
          />
          <Button type='primary' className='btn--warning'>
            Đánh giá
          </Button>
        </Space>
      </div>
    </Card>
  );
}

export default Header;
