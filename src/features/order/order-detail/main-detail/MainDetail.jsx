import React from 'react';
// import PropTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { Layout, Affix } from 'antd';
import Header from './Header';
import Container from './Container';
import Footer from './Footer';

MainDetail.propTypes = {};

function MainDetail(props) {
  const history = useHistory();

  const goBack = () => history.goBack();

  return (
    <Layout
      style={{
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
      }}
    >
      <Affix offsetTop={0}>
        <Header goBack={goBack} />
      </Affix>
      <Container />
      <Footer />
    </Layout>
  );
}

export default MainDetail;
