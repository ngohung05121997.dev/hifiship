import React from 'react';
// import PropTypes from 'prop-types';
import { Row, Col, Spin } from 'antd';
import LayoutPage from 'app/layout/LayoutPage';
import MainDetail from './main-detail/MainDetail';
import Map from './map/Map';

OrderDetail.propTypes = {};

function OrderDetail(props) {
  return (
    <LayoutPage>
      <Spin spinning={false}>
        <Row style={{ height: '100%' }}>
          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
            <MainDetail />
          </Col>
          <Col xs={24} sm={24} md={16} lg={16} xl={16}>
            <Map
              sender={{ lat: 20.9734042, lng: 105.8498541 }}
              receiver={{ lat: 21.018517, lng: 105.7928367 }}
            />
          </Col>
        </Row>
      </Spin>
    </LayoutPage>
  );
}

export default OrderDetail;
