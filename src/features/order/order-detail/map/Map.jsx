import React from 'react';
import PropTypes from 'prop-types';
import GoogleMap from 'app/layout/commons/Map';

const Map = ({ sender, receiver }) => {
  return <GoogleMap sender={sender} receiver={receiver} />;
};

Map.propTypes = {
  sender: PropTypes.object.isRequired,
  receiver: PropTypes.object.isRequired,
};

Map.defaultProps = {};

export default Map;
