import React, { useEffect } from 'react';
import './create-order.style.css';
import { Row, Col, Spin } from 'antd';
import LayoutPage from 'app/layout/LayoutPage';
import Map from './map/Map';
import { useDispatch } from 'react-redux';
import MainAction from './main/MainAction';
import {
  getFeeList,
  clear as clearConstant,
} from 'features/constant/constant.slice';
import { clear as clearOrder } from 'features/order/order.slice';
import useAsync from 'app/hooks/useAsync';
import { loadingTypes } from 'app/utils/config';

CreateOrder.propTypes = {};

const { GET_FEE_LIST } = loadingTypes.constant;

function CreateOrder(props) {
  const dispatch = useDispatch();
  const { loading: getFeeListLoading } = useAsync(GET_FEE_LIST);

  useEffect(() => {
    initCreateOrder();

    return () => {
      dispatch(clearConstant());
      dispatch(clearOrder());
    };

    // eslint-disable-next-line
  }, []);

  const initCreateOrder = async () => {
    await dispatch(getFeeList());
  };
  return (
    <LayoutPage>
      <Spin spinning={getFeeListLoading}>
        <Row style={{ height: '100%' }}>
          <Col xs={24} sm={24} md={8} lg={8} xl={8}>
            <MainAction />
          </Col>
          <Col xs={24} sm={24} md={16} lg={16} xl={16}>
            <Map
              sender={{ lat: 20.9734042, lng: 105.8498541 }}
              receiver={{ lat: 21.018517, lng: 105.7928367 }}
            />
          </Col>
        </Row>
      </Spin>
    </LayoutPage>
  );
}

export default CreateOrder;
