import React, { Fragment } from 'react';
// import PropTypes from 'prop-types';
import { Card, Typography, Timeline, Button } from 'antd';
import { DotIconPrimary } from 'app/layout/commons/Icons';
import { DotIconWarning } from 'app/layout/commons/Icons';
import { PlusCircleFilled } from '@ant-design/icons';
import UserInfo from './UserInfo';
import AddUserForm from './AddUserForm';
import { useState } from 'react';

const { Text } = Typography;

Step1.propTypes = {};

function Step1(props) {
  const [users, setUsers] = useState([
    {
      data: {
        name: 'Hung',
        phone: '0966382597',
        address: '',
      },
      editing: true,
    },
  ]);

  const handleAddSender = ({ sender_address }) => {
    const newUsers = users.map((user, index) =>
      index === 0
        ? {
            data: {
              ...user.data,
              address: sender_address,
            },
            editing: false,
          }
        : user
    );
    setUsers(newUsers);
  };

  const handleAddReceiver = receiverCredentials => {
    const { receiverIndex, ...receiver } = receiverCredentials;
    const newUsers = users.map((user, index) =>
      index === receiverIndex
        ? {
            data: {
              name: receiver.receiver_name,
              phone: receiver.receiver_phone,
              address: receiver.receiver_address,
              cod: receiver.cod,
              note: receiver.note,
            },
            editing: false,
          }
        : user
    );

    setUsers(newUsers);
  };

  const editSender = () => {
    const newUsers = users.map((user, index) =>
      index === 0 ? { ...user, editing: true } : user
    );

    setUsers(newUsers);
  };

  const removeReceiver = receiverIndex => {
    const newUsers = users.filter((user, index) => index !== receiverIndex);

    setUsers(newUsers);
  };

  return (
    <Fragment>
      <Card.Meta
        description={
          <Text>
            <Text>Lộ trình</Text>{' '}
            <Text strong style={{ fontWeight: 700 }}>
              {users.length <= 1 ? 0 : users.length - 1} điểm giao
            </Text>
          </Text>
        }
        style={{ marginBottom: 12 }}
      />
      <Timeline>
        {users.map((user, index) => {
          return (
            <Timeline.Item
              key={index}
              dot={index === 0 ? <DotIconPrimary /> : <DotIconWarning />}
            >
              {user.editing ? (
                <AddUserForm
                  isSender={index === 0}
                  credentials={
                    index === 0
                      ? {
                          sender_name: user.data.name,
                          sender_phone: user.data.phone,
                        }
                      : {
                          receiver_index: index,
                        }
                  }
                  onFinish={index === 0 ? handleAddSender : handleAddReceiver}
                  onCancel={() => removeReceiver(index)}
                />
              ) : (
                <UserInfo
                  userInfo={{ ...user.data, index }}
                  edit={editSender}
                  remove={removeReceiver}
                />
              )}
            </Timeline.Item>
          );
        })}
        {users.length > 0 && (
          <Timeline.Item dot={<DotIconWarning />}>
            <Button
              block
              icon={<PlusCircleFilled />}
              type='dashed'
              onClick={() =>
                setUsers([
                  ...users,
                  {
                    data: {},
                    editing: true,
                  },
                ])
              }
              style={{
                fontWeight: 700,
                color: '#333',
                backgroundColor: '#f0f0f0',
              }}
            >
              Thêm điểm giao
            </Button>
          </Timeline.Item>
        )}
      </Timeline>
    </Fragment>
  );
}

export default Step1;
