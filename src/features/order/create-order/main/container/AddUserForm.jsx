import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Button, Typography, Space, Card, Row, Col } from 'antd';
import {
  EditFilled,
  MobileOutlined,
  UserOutlined,
  MoreOutlined,
  PlusOutlined,
} from '@ant-design/icons';
import { VNDIcon } from 'app/layout/commons/Icons';
import IconText from 'app/layout/commons/IconText';
import { space } from 'app/utils/config';

const { Text } = Typography;

const baseSpace = space.form.middle;

AddUserForm.propTypes = {
  isSender: PropTypes.bool.isRequired,
  credentials: PropTypes.object,
  onFinish: PropTypes.func.isRequired,
  onCancel: PropTypes.func,
};

AddUserForm.defaultProps = {
  isSender: false,
};

function AddUserForm({ isSender, credentials, onFinish, onCancel }) {
  const [form] = Form.useForm();
  const [receiverIndex, setReceiverIndex] = useState(null);

  useEffect(() => {
    if (isSender) {
      form.setFieldsValue(credentials);
    } else {
      setReceiverIndex(credentials.receiver_index);
    }

    // eslint-disable-next-line
  }, [isSender]);

  const handleFinish = values => {
    if (receiverIndex) {
      values.receiverIndex = receiverIndex;
    }

    onFinish(values);
  };

  return (
    <Card
      bordered
      bodyStyle={{ padding: baseSpace, backgroundColor: '#f0f0f0' }}
    >
      <Text strong style={{ marginBottom: baseSpace }}>
        {isSender ? 'Địa điểm lấy hàng' : 'Nhập địa điểm giao hàng'}
      </Text>
      <Form form={form} onFinish={handleFinish}>
        <Row>
          <Col span={24}>
            <Form.Item
              style={{
                backgroundColor: '#fff',
                padding: baseSpace,
                marginBottom: baseSpace,
              }}
              name={isSender ? 'sender_address' : 'receiver_address'}
              help={
                <IconText
                  space={4}
                  icon={<PlusOutlined style={{ fontSize: 10 }} />}
                  label='Chi tiết địa chỉ'
                  style={{ fontSize: 10 }}
                />
              }
              className='custom-form-item'
            >
              <Input
                addonAfter={<MoreOutlined />}
                allowClear
                prefix={
                  <Text style={{ fontSize: 10, position: 'absolute', top: 4 }}>
                    Địa chỉ
                  </Text>
                }
              />
            </Form.Item>
          </Col>
        </Row>
        <Row gutter={baseSpace}>
          <Col span={12}>
            <Form.Item
              style={{
                backgroundColor: '#fff',
                padding: baseSpace,
                marginBottom: baseSpace,
              }}
              name={isSender ? 'sender_name' : 'receiver_name'}
              className='custom-form-item'
            >
              <Input
                prefix={
                  <Text style={{ fontSize: 10, position: 'absolute', top: 4 }}>
                    {isSender ? 'Tên người gửi' : 'Tên người nhận'}
                  </Text>
                }
                suffix={<UserOutlined style={{ color: '#ccc' }} />}
                style={{ border: 'none', backgroundColor: '#fff' }}
                allowClear={isSender ? false : true}
                disabled={isSender ? true : false}
              />
            </Form.Item>
          </Col>
          <Col span={12}>
            <Form.Item
              style={{
                backgroundColor: '#fff',
                padding: baseSpace,
                marginBottom: baseSpace,
              }}
              name={isSender ? 'sender_phone' : 'receiver_phone'}
              className='custom-form-item'
            >
              <Input
                prefix={
                  <Text style={{ fontSize: 10, position: 'absolute', top: 4 }}>
                    Số điện thoại
                  </Text>
                }
                suffix={<MobileOutlined style={{ color: '#ccc' }} />}
                style={{ border: 'none', backgroundColor: '#fff' }}
                allowClear={isSender ? false : true}
                disabled={isSender ? true : false}
              />
            </Form.Item>
          </Col>
        </Row>
        {!isSender && (
          <Row gutter={baseSpace}>
            <Col span={12}>
              <Form.Item
                style={{
                  backgroundColor: '#fff',
                  padding: baseSpace,
                  marginBottom: baseSpace,
                }}
                name='cod'
                className='custom-form-item'
              >
                <Input
                  prefix={
                    <Text
                      style={{ fontSize: 10, position: 'absolute', top: 4 }}
                    >
                      Thu hộ (COD)
                    </Text>
                  }
                  suffix={<VNDIcon />}
                  style={{ border: 'none', backgroundColor: '#fff' }}
                  allowClear
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item
                style={{
                  backgroundColor: '#fff',
                  padding: baseSpace,
                  marginBottom: baseSpace,
                }}
                name='note'
                className='custom-form-item'
              >
                <Input
                  prefix={
                    <Text
                      style={{ fontSize: 10, position: 'absolute', top: 4 }}
                    >
                      Ghi chú
                    </Text>
                  }
                  suffix={<EditFilled style={{ color: '#ccc' }} />}
                  style={{ border: 'none', backgroundColor: '#fff' }}
                  allowClear
                />
              </Form.Item>
            </Col>
          </Row>
        )}

        <Row>
          <Form.Item style={{ marginBottom: baseSpace }}>
            <Space size={baseSpace}>
              <Button
                type='primary'
                style={{ fontWeight: 500 }}
                htmlType='submit'
              >
                Xác nhận
              </Button>
              {!isSender && (
                <Button style={{ fontWeight: 500 }} onClick={onCancel}>
                  Hủy
                </Button>
              )}
            </Space>
          </Form.Item>
        </Row>
      </Form>
    </Card>
  );
}

export default AddUserForm;
