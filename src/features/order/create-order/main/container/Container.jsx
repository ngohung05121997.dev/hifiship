import React from 'react';
import './container.style.css';
import PropTypes from 'prop-types';
import { Card } from 'antd';
import { getElementByStep } from 'app/utils/helper';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';

Container.propTypes = {
  step: PropTypes.number.isRequired,
};

function Container({ step }) {
  const listElementResult = [
    {
      step: 1,
      component: <Step1 />,
    },
    {
      step: 2,
      component: <Step2 />,
    },
    {
      step: 3,
      component: <Step3 />,
    },
  ];
  return (
    <Card style={{ flex: 1 }} bodyStyle={{ padding: 12, height: '100%' }}>
      {getElementByStep(step, listElementResult)}
    </Card>
  );
}

export default Container;
