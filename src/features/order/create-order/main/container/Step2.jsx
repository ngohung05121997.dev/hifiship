import React, { useState } from 'react';
// import PropTypes from 'prop-types';
import {
  Card,
  Input,
  DatePicker,
  Typography,
  Space,
  Checkbox,
  Row,
  Col,
  Button,
} from 'antd';
import { GoBackIcon } from 'app/layout/commons/Icons';
import {
  InfoCircleFilled,
  WalletFilled,
  MinusCircleFilled,
  PlusCircleFilled,
} from '@ant-design/icons';

const { Text } = Typography;
const { TextArea } = Input;

Step2.propTypes = {};

const InputNumberCustom = () => {
  const [number, setNumber] = useState(0);

  const increase = () => setNumber(number + 1);

  const decrease = () => {
    if (number > 0) {
      setNumber(number - 1);
    }
  };

  return (
    <Space>
      <Button
        icon={<MinusCircleFilled />}
        type='link'
        shape='circle'
        onClick={decrease}
      />
      <Text strong>{number}</Text>
      <Button
        icon={<PlusCircleFilled style={{ color: '#1c76bc' }} />}
        type='link'
        shape='circle'
        onClick={increase}
      />
    </Space>
  );
};

function Step2(props) {
  return (
    <div
      style={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
        height: '100%',
      }}
    >
      <div style={{ width: '100%' }}>
        <Card.Meta
          description={<Text>Chọn thêm dịch vụ</Text>}
          style={{ marginBottom: 12 }}
        />
        <Card.Meta
          avatar={<GoBackIcon />}
          description={
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}
            >
              <Space size={0} direction='vertical'>
                <Text strong>
                  Quay lại điểm lấy hàng <InfoCircleFilled />
                </Text>
                <Text style={{ opacity: 0.9 }}>20.000 đ</Text>
              </Space>
              <Checkbox />
            </div>
          }
        />
        <Card.Meta
          avatar={<WalletFilled />}
          description={
            <div
              style={{
                display: 'flex',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
              }}
            >
              <Space size={4} direction='vertical'>
                <Text strong>
                  Hỗ trợ tài xế <InfoCircleFilled />
                </Text>
                <Text style={{ opacity: 0.9 }}>5.000 đ</Text>
              </Space>
              <InputNumberCustom />
            </div>
          }
        />
      </div>
      <div>
        <Row gutter={6}>
          <Col span={12}>
            <Row gutter={[6, 6]}>
              <Col span={24}>
                <DatePicker
                  placeholder='Thời gian lấy hàng'
                  style={{ width: '100%' }}
                />
              </Col>
              <Col span={24}>
                <Input placeholder='Mã khuyến mãi' />
              </Col>
            </Row>
          </Col>
          <Col span={12}>
            <TextArea rows={3} placeholder='Ghi chú cho tài xế' />
          </Col>
        </Row>
      </div>
    </div>
  );
}

export default Step2;
