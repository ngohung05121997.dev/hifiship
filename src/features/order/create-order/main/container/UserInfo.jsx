import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Button, Typography, Space, Card } from 'antd';
import { EditFilled, CloseOutlined } from '@ant-design/icons';
import NumberFormat from 'react-number-format';
import { space } from 'app/utils/config';

const { Text } = Typography;

const baseSpace = space.form.middle;

UserInfo.propTypes = {
  userInfo: PropTypes.object,
  edit: PropTypes.func,
  remove: PropTypes.func,
};

UserInfo.defaultProps = {
  userInfo: {
    name: 'hung',
    phone: '0966382597',
    address: 'HH02-D Thanh Hà, Cự Khê, Thanh Oai, Hà Đông',
  },
};

function UserInfo({ userInfo, edit, remove }) {
  const handleEdit = () => {
    edit();
  };

  const handleRemove = () => {
    remove(userInfo.index);
  };
  return (
    <Card bodyStyle={{ padding: baseSpace }}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'flex-start',
        }}
      >
        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
          }}
        >
          <Text strong>
            <Space>
              <Text style={{ fontWeight: 700 }}>{userInfo.name}</Text>
              <Text>|</Text>
              <Text style={{ fontWeight: 700 }}>{userInfo.phone}</Text>
              {userInfo.cod && (
                <Fragment>
                  <Text>|</Text>
                  <Text style={{ fontWeight: 700 }}>
                    COD
                    <NumberFormat
                      value={userInfo.cod}
                      thousandSeparator=','
                      suffix='đ'
                      displayType='text'
                    />
                  </Text>
                </Fragment>
              )}
            </Space>
          </Text>
          <Text>{userInfo.address}</Text>
          {userInfo.note && (
            <Text style={{ fontSize: 11 }}>(Note: {userInfo.note})</Text>
          )}
        </div>
        <Button
          icon={userInfo.cod ? <CloseOutlined /> : <EditFilled />}
          shape='circle'
          type='link'
          style={{ color: '#333' }}
          onClick={userInfo.cod ? handleRemove : handleEdit}
        />
      </div>
    </Card>
  );
}

export default UserInfo;
