import React, { Fragment } from 'react';
// import PropTypes from 'prop-types';
import { Card, Typography, List, Space, Tag } from 'antd';
import IconText from 'app/layout/commons/IconText';
import { DotIconPrimary } from 'app/layout/commons/Icons';
import { DotIconWarning } from 'app/layout/commons/Icons';

const { Text } = Typography;

Step3.propTypes = {};

function Step3(props) {
  return (
    <Fragment>
      <Card.Meta
        description={<Text>Chọn thêm dịch vụ</Text>}
        style={{ marginBottom: 12 }}
      />
      <List
        dataSource={[1, 2, 3]}
        renderItem={(item, index) => (
          <List.Item key={index} style={{ borderBottom: 'none' }}>
            <Card bordered style={{ width: '100%' }} bodyStyle={{ padding: 0 }}>
              <div style={{ padding: 8, borderBottom: '1px solid #f0f0f0' }}>
                <Space size={4}>
                  <Text strong style={{ fontSize: 12 }}>
                    SIÊU RẺ
                  </Text>
                  <Text style={{ fontSize: 12 }}>|</Text>
                  <Text style={{ fontSize: 12 }}>#HF005502</Text>
                  <Text style={{ fontSize: 12 }}>|</Text>
                  <Text style={{ fontSize: 12 }}>12-07-2019</Text>
                  <Text style={{ fontSize: 12 }}>|</Text>
                  <Text style={{ fontSize: 12 }}>14:22</Text>
                  <Text style={{ fontSize: 12 }}>|</Text>
                  <Text type='warning' style={{ fontSize: 12 }}>
                    15.000 đ
                  </Text>
                </Space>
              </div>
              <div
                style={{ padding: 8, display: 'flex', flexDirection: 'column' }}
              >
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItem: 'center',
                    marginBottom: 6,
                  }}
                >
                  <IconText
                    space={4}
                    icon={<DotIconPrimary />}
                    label='28a, Lê Trọng Tấn, Hà Đông'
                  />
                  <Tag style={{ padding: '2px 4px', borderRadius: 12 }}>
                    Nguyễn Dũng 84915679986
                  </Tag>
                </div>
                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItem: 'center',
                  }}
                >
                  <IconText
                    space={4}
                    icon={<DotIconWarning />}
                    label='số 5 49/64 Huỳnh Thúc Kháng'
                  />
                  <Tag style={{ padding: '2px 4px', borderRadius: 12 }}>
                    Hưng 84966382597
                  </Tag>
                </div>
              </div>
            </Card>
          </List.Item>
        )}
      />
    </Fragment>
  );
}

export default Step3;
