import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Layout, Affix } from 'antd';
import Header from './header/Header';
import { useEffect } from 'react';
import { setCurrentEditing } from 'features/order/order.slice';
import Container from './container/Container';
import Footer from './footer/Footer';
import { useState } from 'react';

MainAction.propTypes = {};

function MainAction(props) {
  const dispatch = useDispatch();
  const [step, setStep] = useState(1);
  const { feeList } = useSelector(state => state.constant);
  const { currentEditing } = useSelector(state => state.order);

  useEffect(() => {
    if (feeList.length > 0) {
      init();
    }

    // eslint-disable-next-line
  }, [feeList]);

  const init = async () => {
    await dispatch(setCurrentEditing({ currentEditing: { fee: feeList[0] } }));
  };

  const gotoNextStep = () => setStep(step + 1);

  const gotoPreviousStep = () => setStep(step - 1);

  const createNewOrder = () => setStep(1);

  return (
    <Layout
      style={{
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
      }}
    >
      <Affix offsetTop={0}>
        <Header
          feeList={feeList}
          selectedFee={currentEditing ? currentEditing.fee : {}}
          step={step}
          goBack={gotoPreviousStep}
        />
      </Affix>
      <Container step={step} />
      <Affix offsetBottom={0}>
        <Footer
          step={step}
          next={gotoNextStep}
          goBack={gotoPreviousStep}
          createNewOrder={createNewOrder}
        />
      </Affix>
    </Layout>
  );
}

export default MainAction;
