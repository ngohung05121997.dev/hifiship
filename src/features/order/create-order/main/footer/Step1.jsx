import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Button, Typography } from 'antd';
import { InfoCircleFilled } from '@ant-design/icons';

const { Text } = Typography;

Step1.propTypes = {
  next: PropTypes.func.isRequired,
};

function Step1({ next }) {
  return (
    <Fragment>
      <Text type='warning'>
        32.000 đ
        <InfoCircleFilled />
      </Text>
      <Button block type='primary' onClick={next}>
        Tiếp tục
      </Button>
    </Fragment>
  );
}

export default Step1;
