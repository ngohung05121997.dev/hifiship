import React from 'react';
import PropTypes from 'prop-types';
import { Typography, Button, Space } from 'antd';
import { InfoCircleFilled } from '@ant-design/icons';

const { Text } = Typography;

Step3.propTypes = {
  next: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  createNewOrder: PropTypes.func.isRequired,
};

function Step3({ next, goBack, createNewOrder }) {
  return (
    <Space direction='vertical' style={{ width: '100%' }}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
        }}
      >
        <Text style={{ fontSize: 11 }}>Tổng tiền</Text>
        <Text type='warning'>
          180.000đ
          <InfoCircleFilled />
        </Text>
      </div>
      <Button
        block
        type='primary'
        style={{ fontWeight: 500 }}
        onClick={createNewOrder}
      >
        Tạo đơn mới
      </Button>
      <Button block type='primary' style={{ fontWeight: 500 }}>
        Kiểm tra đơn hàng
      </Button>
    </Space>
  );
}

export default Step3;
