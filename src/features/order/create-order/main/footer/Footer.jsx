import React from 'react';
import PropTypes from 'prop-types';
import { Card, Typography } from 'antd';
import { getElementByStep } from 'app/utils/helper';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';

const { Text } = Typography;

Footer.propTypes = {
  step: PropTypes.number.isRequired,
  next: PropTypes.func.isRequired,
  goBack: PropTypes.func.isRequired,
  createNewOrder: PropTypes.func.isRequired,
};

function Footer({ step, next, goBack, createNewOrder }) {
  const listElementResult = [
    {
      step: 1,
      component: <Step1 next={next} />,
    },
    {
      step: 2,
      component: <Step2 next={next} />,
    },
    {
      step: 3,
      component: (
        <Step3 next={next} goBack={goBack} createNewOrder={createNewOrder} />
      ),
    },
  ];
  return (
    <Card bordered bodyStyle={{ padding: 0 }}>
      <div
        style={{
          display: 'flex',
          justifyContent: 'center',
          alignItems: 'center',
          flexDirection: 'column',
          padding: 6,
          backgroundColor: '#fff',
        }}
      >
        {getElementByStep(step, listElementResult)}

        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Text type='danger' style={{ textAlign: 'center', fontSize: 11 }}>
            *Lưu ý: Không được giao cho tài xế mà không phải tài xế trong ảnh.
          </Text>
          <Text type='danger' style={{ textAlign: 'center', fontSize: 11 }}>
            Chúng tôi sẽ không chịu trách nhiệm khi bạn giao đơn hàng cho người
            lạ hoặc thỏa thuận ngoài với ship.
          </Text>
        </div>
      </div>
    </Card>
  );
}

export default Footer;
