import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { Select, Typography, Button } from 'antd';
import { MoneyIcon } from 'app/layout/commons/Icons';
import IconText from 'app/layout/commons/IconText';
import { WalletFilled, InfoCircleFilled } from '@ant-design/icons';

const { Option } = Select;
const { Text } = Typography;

Step2.propTypes = {
  next: PropTypes.func.isRequired,
};

function Step2({ next }) {
  return (
    <Fragment>
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          width: '100%',
          marginBottom: 6,
        }}
      >
        <Select defaultValue='Tiền mặt'>
          <Option key='Tiền mặt'>
            <IconText icon={<MoneyIcon />} label='Tiền mặt' />
          </Option>
          <Option key='Tài khoản'>
            <IconText
              icon={<WalletFilled className='primary-color' />}
              label='Tài khoản'
            />
          </Option>
        </Select>
        <Text type='warning'>
          0 đ
          <InfoCircleFilled />
        </Text>
      </div>
      <Button block type='primary' onClick={next}>
        Đặt đơn
      </Button>
    </Fragment>
  );
}

export default Step2;
