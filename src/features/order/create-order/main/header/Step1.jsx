import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Avatar, Typography, Popover, Button } from 'antd';
import { InfoCircleFilled } from '@ant-design/icons';
import FeeListHeader from './FeeListHeader';
import FeeListContainer from './FeeListContainer';

const { Text } = Typography;

Step1.propTypes = {
  feeList: PropTypes.array.isRequired,
};

Step1.defaultProps = {
  feeList: [],
};

function Step1({ feeList }) {
  const [visibleFeeList, setVisibleFeeList] = useState(false);

  const toggleFeeList = () => setVisibleFeeList(!visibleFeeList);
  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <Avatar
        src=''
        size={50}
        style={{ border: '1px solid #1c76bc', marginRight: 8 }}
      />
      <div
        style={{
          display: 'flex',
          justifyContent: 'space-between',
          alignItems: 'center',
          flex: 1,
        }}
      >
        <div
          style={{
            display: 'flex',
            justifyContent: 'space-between',
            alignItems: 'flex-start',
            flexDirection: 'column',
          }}
        >
          <Text style={{ fontSize: 11 }}>
            Siêu rẻ <InfoCircleFilled />{' '}
          </Text>
          <Text style={{ fontSize: 8 }}>
            Giao hàng tối đa 2 giờ, tiết kiệm 20%
          </Text>
        </div>
        <div>
          <Popover
            title={<FeeListHeader toggleFeeList={toggleFeeList} />}
            content={<FeeListContainer feeList={feeList} />}
            placement='rightTop'
            trigger='click'
            overlayClassName='fee-list-popover'
            visible={visibleFeeList}
          >
            <Button style={{ fontSize: 8 }} onClick={toggleFeeList}>
              Đổi dịch vụ
            </Button>
          </Popover>
        </div>
      </div>
    </div>
  );
}

export default Step1;
