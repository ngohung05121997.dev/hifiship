import React from 'react';
import PropTypes from 'prop-types';
import { Button, Typography } from 'antd';
import { LeftOutlined } from '@ant-design/icons';

const { Text } = Typography;

FeeListHeader.propTypes = {
  toggleFeeList: PropTypes.func.isRequired,
};

function FeeListHeader({ toggleFeeList }) {
  return (
    <div style={{ display: 'flex', alignItems: 'center' }}>
      <Button
        icon={<LeftOutlined />}
        type='link'
        shape='circle'
        onClick={toggleFeeList}
      />
      <Text style={{ fontWeight: 500 }}>Tất cả dịch vụ</Text>
    </div>
  );
}

export default FeeListHeader;
