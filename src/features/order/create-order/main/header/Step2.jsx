import React from 'react';
import PropTypes from 'prop-types';
import { Button, Tag, Typography } from 'antd';
import {
  ArrowLeftOutlined,
  WalletFilled,
  InfoCircleFilled,
} from '@ant-design/icons';

const { Text } = Typography;

Step2.propTypes = {
  goBack: PropTypes.func.isRequired,
};

function Step2({ goBack }) {
  return (
    <div>
      <Button
        type='link'
        shape='circle'
        icon={<ArrowLeftOutlined />}
        style={{ marginRight: 4 }}
        onClick={goBack}
      />
      <Tag
        icon={<WalletFilled />}
        style={{ padding: '2px 8px', borderRadius: 12 }}
      >
        <Text>
          Siêu rẻ <InfoCircleFilled />
        </Text>
      </Tag>
    </div>
  );
}

export default Step2;
