import React from 'react';
import PropTypes from 'prop-types';
import { List, Card, Typography } from 'antd';
import { WalletFilled } from '@ant-design/icons';

const { Text } = Typography;

FeeListContainer.propTypes = {
  feeList: PropTypes.array.isRequired,
};

FeeListContainer.defaultProps = {
  feeList: [],
};

function FeeListContainer({ feeList }) {
  return (
    <List
      dataSource={feeList}
      renderItem={fee => (
        <List.Item key={fee.id}>
          <Card bordered={false} bodyStyle={{ padding: 8 }}>
            <Card.Meta
              description={
                <div
                  style={{
                    display: 'flex',
                    alignItems: 'center',
                  }}
                >
                  <div style={{ marginRight: 8 }}>
                    <WalletFilled />
                  </div>
                  <div
                    style={{
                      flex: 1,
                      display: 'flex',
                      alignItems: 'center',
                    }}
                  >
                    <div
                      style={{
                        display: 'flex',
                        flexDirection: 'column',
                        flex: 1,
                        marginRight: 8,
                      }}
                    >
                      <Text
                        style={{
                          fontSize: 11,
                          fontWeight: 500,
                        }}
                      >
                        Siêu tốc
                      </Text>
                      <Text style={{ fontSize: 8 }}>
                        Giao hàng nội thành trong 1 giờ
                      </Text>
                    </div>
                    <div>
                      <Text
                        type='warning'
                        style={{
                          fontSize: 11,
                          fontWeight: 500,
                        }}
                      >
                        32.000
                      </Text>
                    </div>
                  </div>
                </div>
              }
            />
          </Card>
        </List.Item>
      )}
    />
  );
}

export default FeeListContainer;
