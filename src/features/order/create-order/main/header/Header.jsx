import React from 'react';
import PropTypes from 'prop-types';
import './header.style.css';
import { Card, Typography } from 'antd';
import { getElementByStep } from 'app/utils/helper';
import Step1 from './Step1';
import Step2 from './Step2';
import Step3 from './Step3';

const { Text } = Typography;

Header.propTypes = {
  feeList: PropTypes.array.isRequired,
  selectedFee: PropTypes.object.isRequired,
  step: PropTypes.number.isRequired,
  goBack: PropTypes.func.isRequired,
};

Header.defaultProps = {
  feeList: [],
  selectedFee: {},
};

function Header({ feeList, selectedFee, step, goBack }) {
  const listElementResult = [
    {
      step: 1,
      component: <Step1 feeList={feeList} />,
    },
    {
      step: 2,
      component: <Step2 goBack={goBack} />,
    },
    {
      step: 3,
      component: <Step3 />,
    },
  ];

  return (
    <Card bordered bodyStyle={{ padding: 12 }}>
      <Card.Meta
        title={<Text strong>Đơn hàng mới</Text>}
        style={{ marginBottom: 12 }}
      />
      {getElementByStep(step, listElementResult)}
    </Card>
  );
}

export default Header;
