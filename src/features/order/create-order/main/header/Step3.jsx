import React from 'react';
// import PropTypes from 'prop-types';
import { Tag, Typography } from 'antd';
import { WalletFilled, InfoCircleFilled } from '@ant-design/icons';
// import {formatDate} from 'app/utils/helper'

const { Text } = Typography;

Step3.propTypes = {};

function Step3(props) {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'flex-start',
      }}
    >
      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
        }}
      >
        <Tag
          icon={<WalletFilled />}
          style={{
            padding: '2px 8px',
            borderRadius: 12,
            backgroundColor: '#f0f0f0',
          }}
        >
          Siêu rẻ {<InfoCircleFilled />}
        </Tag>
        <Text style={{ fontSize: 11 }}>12-07-2019 - 14:22</Text>
      </div>
      <Tag style={{ backgroundColor: '#f0f0f0', marginRight: 0 }}>
        Đang tìm ship
      </Tag>
    </div>
  );
}

export default Step3;
