import { createSlice } from '@reduxjs/toolkit';
import {
  asyncActionStart,
  asyncActionError,
  asyncActionFinish,
} from 'features/async/async.slice';
import { loadingTypes } from 'app/utils/config';
import constantApi from 'api/constant.api';

const { GET_FEE_LIST, GET_STATUS_LIST } = loadingTypes.constant;

const constantSlice = createSlice({
  name: 'constant',
  initialState: {
    feeList: [],
    statusList: [],
  },
  reducers: {
    setFeeList: (state, action) => {
      state.feeList = action.payload.feeList;
    },
    setStatusList: (state, action) => {
      state.statusList = action.payload.statusList;
    },
    clear: state => {
      state.feeList = [];
      state.statusList = [];
    },
  },
});

export const { clear, setFeeList, setStatusList } = constantSlice.actions;

export const getFeeList = () => async dispatch => {
  dispatch(asyncActionStart({ loadingType: GET_FEE_LIST }));

  try {
    const response = await constantApi.getFeeList();

    if (response.status === 'success') {
      dispatch(setFeeList({ feeList: response.data }));
      dispatch(asyncActionFinish());
    } else {
      dispatch(asyncActionError());
    }
  } catch (err) {
    console.log(err);
    dispatch(asyncActionError());
  }
};

export const getStatusList = () => async dispatch => {
  dispatch(asyncActionStart({ loadingType: GET_STATUS_LIST }));

  try {
    const response = await constantApi.getStatusList();

    if (response.status === 'success') {
      dispatch(setStatusList({ statusList: response.data }));
      dispatch(asyncActionFinish());
    } else {
      dispatch(asyncActionError());
    }
  } catch (err) {
    console.log(err);
    dispatch(asyncActionError());
  }
};

export default constantSlice.reducer;
