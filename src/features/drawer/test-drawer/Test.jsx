import React from 'react';
import { useDispatch } from 'react-redux';
import { Drawer } from 'antd';
import { closeDrawer } from '../drawer.slice';

const TestDrawer = props => {
  const dispatch = useDispatch();

  const handleCancel = () => dispatch(closeDrawer());

  return (
    <Drawer
      title='Basic Drawer'
      placement='right'
      onClose={handleCancel}
      visible={true}
    >
      <p>Some contents...</p>
      <p>Some contents...</p>
      <p>Some contents...</p>
    </Drawer>
  );
};

export default TestDrawer;
