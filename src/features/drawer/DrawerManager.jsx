import React from 'react';
import { useSelector } from 'react-redux';

import TestDrawer from './test-drawer/Test';
import MenuDrawer from './sidebar/MenuDrawer';

const drawerLookup = {
  TestDrawer,
  MenuDrawer,
};

const DrawerManager = props => {
  let currentDrawer = useSelector(state => state.drawer);
  let renderDrawer;

  if (currentDrawer.drawerType) {
    const { drawerType, drawerProps } = currentDrawer;
    const DrawerComponent = drawerLookup[drawerType];

    renderDrawer = <DrawerComponent {...drawerProps} />;
  }

  return <span>{renderDrawer}</span>;
};

export default DrawerManager;
