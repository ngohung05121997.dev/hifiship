import { createSlice } from '@reduxjs/toolkit';

export const drawerSlice = createSlice({
  name: 'drawer',
  initialState: {
    drawerType: null,
    drawerProps: null,
  },
  reducers: {
    openDrawer: (state, action) => {
      const { drawerType, drawerProps } = action.payload;
      state.drawerType = drawerType;
      state.drawerProps = drawerProps;
    },
    closeDrawer: state => {
      state.drawerType = null;
      state.drawerProps = null;
    },
  },
});

export const { openDrawer, closeDrawer } = drawerSlice.actions;

export default drawerSlice.reducer;
