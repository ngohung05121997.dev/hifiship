import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Menu, Typography, Avatar, Dropdown } from 'antd';
import defaultUser from 'assets/images/user/defaultUser.jpg';
import {
  CaretDownOutlined,
  UserOutlined,
  PoweroffOutlined,
} from '@ant-design/icons';

const { Text } = Typography;

MenuDrawerHeader.propTypes = {
  authUser: PropTypes.object.isRequired,
  logout: PropTypes.func.isRequired,
};

function MenuDrawerHeader({ authUser, logout }) {
  const { avatar, name, phone } = authUser.data;

  const menu = (
    <Menu>
      <Menu.Item icon={<UserOutlined />}>
        <Link to='/main/profile'>Thông tin cá nhân</Link>
      </Menu.Item>
      <Menu.Divider />
      <Menu.Item icon={<PoweroffOutlined />} onClick={logout}>
        Thoát ra
      </Menu.Item>
    </Menu>
  );

  return (
    <Dropdown overlay={menu} trigger={['click']}>
      <div
        style={{
          display: 'flex',
          alignItems: 'center',
          margin: '4px 4px 8px 4px',
          padding: '4px 18px',
          backgroundColor: 'rgba(224, 196, 196, 0.34)',
          borderRadius: 4,
          cursor: 'pointer',
        }}
      >
        <Avatar alt='avatar' src={avatar || defaultUser} />

        <div
          style={{
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'flex-start',
            marginLeft: 12,
            flex: 1,
          }}
        >
          <Text
            style={{
              color: '#fff',
              display: 'flex',
              justifyContent: 'space-between',
              alignItems: 'center',
              width: '100%',
            }}
          >
            {name} <CaretDownOutlined />
          </Text>
          <Text style={{ color: '#fff' }}>{phone}</Text>
        </div>
      </div>
    </Dropdown>
  );
}

export default MenuDrawerHeader;
