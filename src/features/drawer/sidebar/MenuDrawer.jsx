import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Drawer, Menu, Typography } from 'antd';
import { closeDrawer } from '../drawer.slice';
import { sidebarMenus } from 'app/utils/sidebarConfig';
import { Link } from 'react-router-dom';
import MenuDrawerHeader from './MenuDrawerHeader';
import { logout } from 'features/auth/auth.slice';

const { Text } = Typography;

MenuDrawer.propTypes = {};

function MenuDrawer(props) {
  const dispatch = useDispatch();
  const { user: authUser } = useSelector(state => state.auth);

  const handleCancel = () => dispatch(closeDrawer());

  const handleLogout = () => {
    dispatch(logout());
    dispatch(closeDrawer());
  };

  return (
    <Drawer
      title={<Text style={{ color: '#fff' }}>HIFISHIP</Text>}
      placement='left'
      onClose={handleCancel}
      visible={true}
      width={250}
      bodyStyle={{ backgroundColor: '#001529', padding: 0 }}
      headerStyle={{
        backgroundColor: '#001529',
        borderRadius: 0,
        borderBottom: 'none',
      }}
    >
      <MenuDrawerHeader authUser={authUser} logout={handleLogout} />

      <Menu theme='dark' mode='inline'>
        {sidebarMenus.map(menu => (
          <Menu.Item key={menu.key} icon={menu.icon}>
            <Link to={menu.path}>{menu.label}</Link>
          </Menu.Item>
        ))}
      </Menu>
    </Drawer>
  );
}

export default MenuDrawer;
