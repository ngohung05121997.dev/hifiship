import React from 'react';
import { useDispatch } from 'react-redux';
import { Modal } from 'antd';
import { closeModal } from '../modal.slice';

const TestModal = props => {
  const dispatch = useDispatch();

  const handleCancel = () => dispatch(closeModal());

  return (
    <Modal title='Basic Modal' visible={true} onCancel={handleCancel}>
      <p>Some contents...</p>
      <p>Some contents...</p>
      <p>Some contents...</p>
    </Modal>
  );
};

export default TestModal;
