import React from 'react';
// import PropTypes from 'prop-types';
import Step4 from 'features/auth/register/Step4';
import { Modal } from 'antd';
import { useDispatch } from 'react-redux';
import { closeModal } from '../modal.slice';

RegisterSuccess.propTypes = {};

function RegisterSuccess(props) {
  const dispatch = useDispatch();

  const handleCancel = () => dispatch(closeModal());

  return (
    <Modal visible={true} onCancel={handleCancel} width={650}>
      <Step4 />
    </Modal>
  );
}

export default RegisterSuccess;
