import React from 'react';
import { useSelector } from 'react-redux';

import TestModal from './test-modal/Test';
import RegisterSuccessModal from './auth/RegisterSuccess';
import QRModal from './wallet/QRModal';
import AddBankInfoModal from './wallet/AddBank';
import AddBankFormModal from './wallet/AddBankFormModal';
import ConfirmAccountModal from './wallet/ConfirmAccountModal';
import UserBankInfoModal from './wallet/UserBankInfoModal';

const modalLookup = {
  TestModal: TestModal,
  RegisterSuccess: RegisterSuccessModal,
  QRModal: QRModal,
  AddBankInfo: AddBankInfoModal,
  AddBankFormModal: AddBankFormModal,
  ConfirmAccountModal: ConfirmAccountModal,
  UserBankInfoModal: UserBankInfoModal,
};

const ModalManager = props => {
  let currentModal = useSelector(state => state.modal);
  let renderModal;

  if (currentModal.modalType) {
    const { modalType, modalProps } = currentModal;
    const ModalComponent = modalLookup[modalType];

    renderModal = <ModalComponent {...modalProps} />;
  }

  return <span>{renderModal}</span>;
};

export default ModalManager;
