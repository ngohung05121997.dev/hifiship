import React from 'react';
import { useDispatch } from 'react-redux';
import { Modal } from 'antd';
import { closeModal, openModal } from '../modal.slice';
import ConfirmAccount from 'features/wallet/add-bank/ConfirmAccount';

ConfirmAccountModal.propTypes = {};

function ConfirmAccountModal(props) {
  const dispatch = useDispatch();

  const handleCancel = () => dispatch(closeModal());

  const handleConfirm = () =>
    dispatch(openModal({ modalType: 'UserBankInfoModal' }));

  return (
    <Modal
      footer={null}
      visible={true}
      width={350}
      onCancel={handleCancel}
      closable={false}
    >
      <ConfirmAccount cancel={handleCancel} confirm={handleConfirm} />
    </Modal>
  );
}

export default ConfirmAccountModal;
