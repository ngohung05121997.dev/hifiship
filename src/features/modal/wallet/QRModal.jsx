import React from 'react';
import { Modal, Typography, Button } from 'antd';
import { useDispatch, useSelector } from 'react-redux';
import { closeModal } from '../modal.slice';
import Price from 'app/layout/commons/Price';
import VnPayLogo from 'assets/images/logo/logo-vnpay.png';
import MomoLogo from 'assets/images/logo/logo-momo.png';
import ZaloPayLogo from 'assets/images/logo/logo-zalopay.png';
import QRCodeImage from 'assets/images/wallet/qr-code.png';

const { Text } = Typography;

QRModal.propTypes = {};

const paymentMethods = {
  VNPay: VnPayLogo,
  Momo: MomoLogo,
  ZaloPay: ZaloPayLogo,
};

function QRModal(props) {
  const dispatch = useDispatch();
  const { price, method: methodName } = useSelector(
    state => state.modal.modalProps
  );

  const handleCancel = () => dispatch(closeModal());

  return (
    <Modal
      visible={true}
      onCancel={handleCancel}
      width={380}
      footer={null}
      bodyStyle={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
        paddingBottom: 12,
      }}
    >
      <Text>Nạp tiền vào tài khoản chính</Text>
      <Text style={{ fontWeight: 500, marginBottom: 12 }} type='warning'>
        <Price priceString={price} />
      </Text>

      <div
        style={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'center',
          justifyContent: 'center',
          position: 'relative',
        }}
      >
        <img
          alt='payment-method'
          src={paymentMethods[methodName]}
          style={{
            width: 80,
            position: 'absolute',
            top: 0,
            left: '50%',
            transform: 'translate(-50%, -50%)',
          }}
        />
        <img
          alt='qr-code'
          src={QRCodeImage}
          style={{ width: '100%', padding: 12, border: '1px solid #ccc' }}
        />
      </div>
      <Text
        style={{
          fontSize: 11,
          width: '70%',
          textAlign: 'center',
          marginBottom: 12,
        }}
      >
        Mở ứng dụng ngân hàng của bạn và quét mã QR code để nạp tiền
      </Text>

      <div style={{ display: 'flex' }}>
        <Button type='link' style={{ flex: 1 }}>
          Những ngân hàng hỗ trợ
        </Button>
        <Button type='link'>Hướng dẫn</Button>
        <Button type='link' danger>
          Hủy bỏ
        </Button>
      </div>
    </Modal>
  );
}

export default QRModal;
