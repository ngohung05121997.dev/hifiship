import React from 'react';
import { useDispatch } from 'react-redux';
import { Modal } from 'antd';
import { closeModal } from '../modal.slice';
import AddBankInfo from 'features/wallet/add-bank/AddBankInfo';

AddBank.propTypes = {};

function AddBank(props) {
  const dispatch = useDispatch();

  const handleCancel = () => dispatch(closeModal());

  return (
    <Modal
      footer={null}
      visible={true}
      closable={false}
      width={350}
      onCancel={handleCancel}
      bodyStyle={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}
    >
      <AddBankInfo cancel={handleCancel} />
    </Modal>
  );
}

export default AddBank;
