import React from 'react';
import { useDispatch } from 'react-redux';
import { Modal } from 'antd';
import { closeModal } from '../modal.slice';
import UserBankInfo from 'features/wallet/add-bank/UserBankInfo';

UserBankInfoModal.propTypes = {};

function UserBankInfoModal(props) {
  const dispatch = useDispatch();

  const handleCancel = () => dispatch(closeModal());

  return (
    <Modal
      footer={null}
      visible={true}
      closable={false}
      width={350}
      onCancel={handleCancel}
      bodyStyle={{
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}
    >
      <UserBankInfo cancel={handleCancel} />
    </Modal>
  );
}

export default UserBankInfoModal;
