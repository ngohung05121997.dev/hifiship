import React from 'react';
import { useDispatch } from 'react-redux';
import { Modal } from 'antd';
import { closeModal } from '../modal.slice';
import AddBankForm from 'features/wallet/add-bank/AddBankForm';

AddBankFormModal.propTypes = {};

function AddBankFormModal(props) {
  const dispatch = useDispatch();

  const handleCancel = () => dispatch(closeModal());

  return (
    <Modal
      footer={null}
      visible={true}
      width={350}
      onCancel={handleCancel}
      closable={false}
    >
      <AddBankForm />
    </Modal>
  );
}

export default AddBankFormModal;
