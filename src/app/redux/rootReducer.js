import counterReducer from 'features/counter/counterSlice';
import asyncReducer from 'features/async/async.slice';
import { reducer as toastrReducer } from 'react-redux-toastr';
import modalReducer from 'features/modal/modal.slice';
import drawerReducer from 'features/drawer/drawer.slice';
import authReducer from 'features/auth/auth.slice';
import constantReducer from 'features/constant/constant.slice';
import orderReducer from 'features/order/order.slice';

const rootReducer = {
  counter: counterReducer,
  async: asyncReducer,
  toastr: toastrReducer,
  modal: modalReducer,
  drawer: drawerReducer,
  auth: authReducer,
  constant: constantReducer,
  order: orderReducer,
};

export default rootReducer;
