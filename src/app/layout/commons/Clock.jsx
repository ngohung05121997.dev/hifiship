import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';
import useClock from 'app/hooks/useClock';

Clock.propTypes = {
  callback: PropTypes.func.isRequired,
  initTime: PropTypes.number.isRequired,
};

Clock.defaultProps = {
  callback: () => console.log('time out'),
  initTime: 200,
};

function Clock({ initTime, callback, ...rest }) {
  const { time } = useClock(initTime, callback);

  return (
    <Typography.Text strong {...rest}>
      {time} s
    </Typography.Text>
  );
}

export default Clock;
