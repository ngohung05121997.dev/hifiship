import React from 'react';
import PropTypes from 'prop-types';
import { EnvironmentFilled } from '@ant-design/icons';
import GoogleMapReact from 'google-map-react';

const zoom = 14;

const Marker = ({ text }) => (
  <div>
    <EnvironmentFilled style={{ color: 'red' }} /> {text}{' '}
  </div>
);

const Map = ({ sender, receiver }) => {
  return (
    <GoogleMapReact
      bootstrapURLKeys={{ key: 'AIzaSyCOn_bhrgWm0Ufa3WAdJNmgOIYjPItdDhs' }}
      defaultCenter={{ lat: sender.lat, lng: sender.lng }}
      defaultZoom={zoom}
    >
      <Marker text='My Marker' lat={sender.lat} lng={sender.lng} />
      <Marker text='Receiver' lat={receiver.lat} lng={receiver.lng} />
    </GoogleMapReact>
  );
};

Map.propTypes = {
  sender: PropTypes.object.isRequired,
  receiver: PropTypes.object.isRequired,
};

Map.defaultProps = {};

export default Map;
