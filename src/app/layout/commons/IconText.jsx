import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';

const { Text } = Typography;

IconText.propTypes = {
  space: PropTypes.number.isRequired,
  label: PropTypes.string.isRequired,
};

IconText.defaultProps = {
  space: 8,
};

function IconText({ space, icon, label, style }) {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'flex-start',
        alignItems: 'center',
      }}
    >
      {icon}
      <Text style={{ marginLeft: space, ...style }}>{label}</Text>
    </div>
  );
}

export default IconText;
