import React from 'react';
import PropTypes from 'prop-types';
import NumberFormat from 'react-number-format';

Price.propTypes = {
  priceString: PropTypes.number.isRequired,
};

function Price({ priceString }) {
  return (
    <NumberFormat
      value={priceString}
      thousandSeparator=','
      suffix='đ'
      displayType='text'
    />
  );
}

export default Price;
