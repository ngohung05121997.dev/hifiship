import React from 'react';
import PropTypes from 'prop-types';
import { Typography } from 'antd';

CenterText.propTypes = {
  error: PropTypes.bool.isRequired,
  text: PropTypes.string.isRequired,
};

CenterText.defaultProps = {
  error: true,
};

const { Text } = Typography;

function CenterText({ error, text }) {
  return (
    <Text
      type={error ? 'danger' : 'primary'}
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
      }}
    >
      {text}
    </Text>
  );
}

export default CenterText;
