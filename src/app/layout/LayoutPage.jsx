import React from 'react';
import { Layout } from 'antd';
import Sidebar from './Sidebar';

LayoutPage.propTypes = {};

function LayoutPage({ children }) {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sidebar />
      <Layout.Content style={{ marginLeft: 80 }}>{children}</Layout.Content>
    </Layout>
  );
}

export default LayoutPage;
