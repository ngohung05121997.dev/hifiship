import React, { useEffect } from 'react';
import './App.css';
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css';
import { Switch, Route, Redirect } from 'react-router-dom';

import renderMenuItem from 'routes';

// import ScrollToTop from './commons/ScollToTop';
import Toastr from './commons/Toastr';
import ModalManager from 'features/modal/ModalManager';
import DrawerManager from 'features/drawer/DrawerManager';
import NotFoundPage from './pages/NotFoundPage';
import { useDispatch, useSelector } from 'react-redux';
import { getAuthUser } from 'features/auth/auth.slice';
import { Spin } from 'antd';
import { loadingTypes } from 'app/utils/config';
import useAsync from 'app/hooks/useAsync';

const { GET_AUTH_USER } = loadingTypes.auth;

function App() {
  const dispatch = useDispatch();
  const { loading: getAuthUserLoading } = useAsync(GET_AUTH_USER);
  const { authenticated } = useSelector(state => state.auth);
  useEffect(() => {
    initApp();

    // eslint-disable-next-line
  }, []);

  const initApp = async () => {
    if (authenticated) {
      await dispatch(getAuthUser());
    }
  };

  return (
    <Spin spinning={getAuthUserLoading} style={{ minHeight: '100vh' }}>
      <div style={{ height: '100%' }}>
        {/* <ScrollToTop /> */}
        <Toastr />
        <ModalManager />
        <DrawerManager />

        <Switch>
          <Route exact path='/' component={() => <Redirect to='/main' />} />
          {renderMenuItem()}
          <Route render={NotFoundPage} />
        </Switch>
      </div>
    </Spin>
  );
}

export default App;
