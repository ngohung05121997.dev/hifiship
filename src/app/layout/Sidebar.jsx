import React from 'react';
// import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux';
import { Layout, Menu, Tooltip } from 'antd';
import { MenuOutlined } from '@ant-design/icons';
import { sidebarMenus } from 'app/utils/sidebarConfig';
import { openDrawer } from 'features/drawer/drawer.slice';
import { Link } from 'react-router-dom';

const { Sider } = Layout;

Sidebar.propTypes = {};

function Sidebar(props) {
  const dispatch = useDispatch();

  const openMenuDrawer = () =>
    dispatch(openDrawer({ drawerType: 'MenuDrawer' }));

  return (
    <Sider
      collapsible
      collapsed={true}
      trigger={null}
      style={{
        overflow: 'auto',
        height: '100vh',
        position: 'fixed',
        left: 0,
      }}
    >
      <Menu theme='dark' defaultSelectedKeys={['1']} mode='inline'>
        <Menu.Item icon={<MenuOutlined />} onClick={openMenuDrawer} />
        {sidebarMenus.map(menu => (
          <Menu.Item
            key={menu.key}
            icon={
              <Tooltip title={menu.label}>
                <Link to={menu.path}>{menu.icon}</Link>
              </Tooltip>
            }
          />
        ))}
      </Menu>
    </Sider>
  );
}

export default Sidebar;
