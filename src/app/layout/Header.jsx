import React, { Fragment } from 'react';
import PropTypes from 'prop-types';
import { PageHeader, Button, Space } from 'antd';
import { BellFilled, ArrowLeftOutlined } from '@ant-design/icons';
import { FileDownloadIcon } from './commons/Icons';

Header.propTypes = {
  title: PropTypes.string.isRequired,
  description: PropTypes.any,
  goBack: PropTypes.func,
  exportRequire: PropTypes.bool.isRequired,
  goBackRequire: PropTypes.bool.isRequired,
};

Header.defaultProps = {
  exportRequire: true,
  goBackRequire: false,
};

function Header({
  title,
  description,
  goBackRequire,
  goBack,
  exportRequire,
  onBack,
  ...rest
}) {
  return (
    <PageHeader
      {...rest}
      title={
        <Fragment>
          {goBackRequire && (
            <Button
              icon={<ArrowLeftOutlined style={{ color: '#111' }} />}
              type='link'
              shape='circle'
              onClick={goBack}
            />
          )}
          {title}
        </Fragment>
      }
      onBack={onBack}
      style={{ background: '#ffffff}' }}
      className='head-layout'
      subTitle={
        <Space>
          {description}
          {exportRequire && (
            <Button icon={<FileDownloadIcon />} className='btn-default'>
              Xuất dữ liệu
            </Button>
          )}
        </Space>
      }
      extra={
        <Button
          icon={<BellFilled />}
          type='link'
          className='fr bg-bell'
          block
        />
      }
    />
  );
}

export default Header;
