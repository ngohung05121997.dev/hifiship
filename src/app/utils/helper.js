import jwtDecode from 'jwt-decode';
import moment from 'moment';

// helper function will be here
const isAuthenticated = () => {
  const token = localStorage.getItem('token');
  if (token) {
    const decodedToken = jwtDecode(token);
    if (decodedToken.exp * 1000 < Date.now()) {
      return false;
    }

    return token;
  }

  return false;
};

const getElementByStep = (step, list) =>
  list.find(item => item.step === step).component;

const formatDate = stringDate => moment(stringDate).format('MM-DD-YYYY-hh:mm');

export { isAuthenticated, getElementByStep, formatDate };
