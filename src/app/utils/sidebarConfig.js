import React from 'react';
import {
  ApartmentOutlined,
  UserOutlined,
  LineChartOutlined,
  PlusCircleFilled,
  ContactsFilled,
  IdcardFilled,
  WalletFilled,
  StarFilled,
  InfoCircleFilled,
} from '@ant-design/icons';
import { CouponIcon } from 'app/layout/commons/Icons';
import { pages } from './config';

const sidebarMenus = [
  {
    icon: <PlusCircleFilled />,
    key: pages.CREATE_ORDER.path,
    path: pages.CREATE_ORDER.path,
    label: 'Tạo đơn hàng',
  },
  {
    icon: <ApartmentOutlined />,
    key: pages.ORDER_HISTORY.path,
    path: pages.ORDER_HISTORY.path,
    label: 'Quản lý đơn hàng',
  },
  {
    icon: <ContactsFilled />,
    key: 'shippers',
    label: 'Danh sách shipper',
    path: '/manage-shippers',
  },
  {
    icon: <IdcardFilled />,
    key: 'customers',
    label: 'Danh sách khách hàng',
    path: '/manage-customers',
  },
  {
    icon: <UserOutlined />,
    key: 'admin',
    label: 'Tài khoản quản trị',
    path: '/manage-admin',
  },
  {
    icon: <WalletFilled />,
    key: pages.MANAGE_WALLET.path,
    path: pages.MANAGE_WALLET.path,
    label: 'Quản lý ví tiền',
  },
  {
    icon: <StarFilled />,
    key: 'ratings',
    label: 'Đánh giá khách hàng',
    path: '/manage-customer-ratings',
  },
  {
    icon: <CouponIcon />,
    key: pages.DISCOUNT_CODE.path,
    path: pages.DISCOUNT_CODE.path,
    label: 'Mã giảm giá',
  },
  {
    icon: <LineChartOutlined />,
    key: 'statistical',
    label: 'Thống kê',
    path: '/statistical',
  },
  {
    icon: <InfoCircleFilled />,
    key: 'helps',
    label: 'Trợ giúp',
    path: '/helps',
  },
];

export { sidebarMenus };
