// all config will be here
export const routeTypes = {
  PUBLIC: 'PUBLIC',
  PRIVATE: 'PRIVATE',
  AUTH: 'AUTH',
};

export const loadingTypes = {
  auth: {
    LOGIN: 'LOGIN',
    REGISTER: 'REGISTER',
    GET_AUTH_USER: 'GET_AUTH_USER',
    CHECK_PHONE: 'CHECK_PHONE',
    VALIDATE_PHONE_NUMBER: 'VALIDATE_PHONE_NUMBER',
    SEND_OTP_CODE: 'SEND_OTP_CODE',
    CHECK_OTP_CODE: 'CHECK_OTP_CODE',
    UPDATE_PASSWORD: 'UPDATE_PASSWORD',
    GET_AUTH_BALANCE: 'GET_AUTH_BALANCE',
  },
  order: {
    CREATE_ORDER: 'CREATE_ORDER',
    GET_ORDER_LIST: 'GET_ORDER_LIST',
    GET_ORDER_DETAIL: 'GET_ORDER_DETAIL',
    CALCULATE_ORDER_FEE: 'CALCULATE_ORDER_FEE',
  },
  constant: {
    GET_FEE_LIST: 'GET_FEE_LIST',
    GET_STATUS_LIST: 'GET_STATUS_LIST',
  },
};

export const pages = {
  HOME: {
    path: '/main',
    exact: true,
    type: routeTypes.PRIVATE,
  },
  LOGIN: {
    path: '/login',
    exact: true,
    type: routeTypes.AUTH,
  },
  REGISTER: {
    path: '/register',
    exact: true,
    type: routeTypes.AUTH,
  },
  FORGET_PASSWORD: {
    path: '/forget-password',
    exact: true,
    type: routeTypes.AUTH,
  },
  POLICY: {
    path: '/main/policy',
    exact: true,
    type: routeTypes.PUBLIC,
  },
  CREATE_ORDER: {
    path: '/main/order/create',
    exact: true,
    type: routeTypes.PRIVATE,
  },
  ORDER_HISTORY: {
    path: '/main/order/history',
    exact: true,
    type: routeTypes.PRIVATE,
  },
  ORDER_DETAILS: {
    path: '/main/order/:orderId',
    exact: true,
    type: routeTypes.PRIVATE,
  },
  MANAGE_WALLET: {
    path: '/main/wallet',
    exact: true,
    type: routeTypes.PRIVATE,
  },
  LIST_CUSTOMER: {
    path: '/manage-customers',
    exact: true,
    type: routeTypes.PRIVATE,
  },
  DETAIL_CUSTOMER: {
    path: '/manage-customers/:customId',
    exact: true,
    type: routeTypes.PRIVATE,
  },
  LIST_SHIPPER: {
    path: '/manage-shippers',
    exact: true,
    type: routeTypes.PRIVATE,
  },
  DISCOUNT_CODE: {
    path: '/discount-code',
    exact: true,
    type: routeTypes.PRIVATE,
  },
  DISCOUNT_CODE_DETAIL: {
    path: '/discount-code/:discountId',
    exact: true,
    type: routeTypes.PRIVATE,
  },
  CREATE_DISCOUNT: {
    path: '/discount-code/create',
    exact: true,
    type: routeTypes.PRIVATE,
  },
};

export const space = {
  form: {
    small: 6,
    middle: 8,
    large: 12,
  },
};
