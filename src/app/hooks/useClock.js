import { useState, useEffect } from 'react';

function useClock(initTime, callback) {
  const [time, setTime] = useState(() => initTime);

  useEffect(() => {
    const clockInterval = setInterval(() => {
      if (time === 0) {
        callback();
      } else {
        setTime(time - 1);
      }
    }, 1000);

    return () => {
      clearInterval(clockInterval);
    };
  }, [time, callback]);

  return { time };
}

export default useClock;
