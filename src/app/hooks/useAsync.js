import { useSelector } from 'react-redux';

const useAsync = type => {
  const asyncResult = {};
  const { loading, loadingType, error } = useSelector(state => state.async);

  asyncResult.loading = type === loadingType ? loading : false;

  if (error) {
    asyncResult.error = error;
  }

  return asyncResult;
};

export default useAsync;
