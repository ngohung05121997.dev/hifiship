import axiosClient from './axiosClient';

const constantApi = {
  getFeeList: () => {
    const url = 'constant/fee';
    return axiosClient.get(url);
  },
  getStatusList: () => {
    const url = '/constant/shipmentStatus';
    return axiosClient.get(url);
  },
};

export default constantApi;
