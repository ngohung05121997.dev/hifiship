import axios from 'axios';
import queryString from 'query-string';
import { isAuthenticated } from 'app/utils/helper';

// import localStorageService from 'app/utils/LocalStorageService';

const baseUrl = process.env.REACT_APP_BASE_URL;

const axiosClient = axios.create({
  baseURL: baseUrl,
  headers: {
    'content-type': 'application/json',
  },
  paramsSerializer: params => queryString.stringify(params),
});

axiosClient.interceptors.request.use(async config => {
  // Handle token here
  const token = isAuthenticated();
  if (token) {
    config.headers['Authorization'] = `Bearer ${token}`;
  }

  return config;
});

axiosClient.interceptors.response.use(
  response => {
    if (response && response.data) {
      return response.data;
    }

    return response;
  },
  err => {
    // throw err;
    if (err && err.response && err.response.data) {
      return err.response.data;
    }

    return err.response;
  }
);

export default axiosClient;
