import axiosClient from './axiosClient';

const login = credentials => {
  const url = '/account/login';
  return axiosClient.post(url, credentials);
};

const register = credentials => {
  const url = '/account/register';
  return axiosClient.post(url, credentials);
};

const checkPhone = phone => {
  const url = `/account/checkPhone/${phone}`;
  return axiosClient.get(url);
};

const checkOtp = () => {
  const url = '';
  return axiosClient.post(url);
};

const sendRegisterCode = phone => {
  const url = '/account/sendCode';
  return axiosClient.post(url, { phone });
};

const forgotPassword = () => {};

const getAuthUser = () => {
  const url = '/account/profile';
  return axiosClient.get(url);
};

const getAuthBalance = () => {
  const url = '/wallet/balance';
  return axiosClient.get(url);
};

const updateProfile = () => {};

const authApi = {
  login,
  register,
  checkPhone,
  checkOtp,
  sendRegisterCode,
  forgotPassword,
  getAuthUser,
  getAuthBalance,
  updateProfile,
};

export default authApi;
