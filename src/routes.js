import React from 'react';
import { Route } from 'react-router-dom';
import PrivateRoute from 'app/layout/commons/PrivateRoute';

import { routeTypes, pages } from './app/utils/config';
import AuthRoute from 'app/layout/commons/AuthRoute';
import Register from 'features/auth/register/Register';
import Login from 'features/auth/login/Login';
import ForgetPassword from 'features/auth/forget-password/ForgetPassword';
import PolicyPage from 'app/layout/pages/PolicyPage';
import HomePage from 'app/layout/pages/HomePage';
import CreateOrder from 'features/order/create-order/CreateOrder';
import OrderHistory from 'features/order/history/OrderHistory';
import OrderDetail from 'features/order/order-detail/OrderDetail';
import ManageWallet from 'features/wallet/manage-wallet/ManageWallet';
import ListCustomer from 'features/managerCustomer/ListCustomer';
import ListShipper from 'features/shipper/ListShipper';
import DetailCustomer from 'features/managerCustomer/DetailCustomer/DetailCustomer';
import DiscountCodeList from 'features/discount-code/list/DiscountCodeList';
import DiscountCodeDetail from 'features/discount-code/detail/DiscountCodeDetail';
import CreateDiscount from 'features/discount-code/create-discount/CreateDiscount';

let routes = [
  {
    ...pages.HOME,
    component: () => <HomePage />,
  },
  {
    ...pages.LOGIN,
    component: () => <Login />,
  },
  {
    ...pages.REGISTER,
    component: () => <Register />,
  },
  {
    ...pages.FORGET_PASSWORD,
    component: () => <ForgetPassword />,
  },
  {
    ...pages.POLICY,
    component: () => <PolicyPage />,
  },
  {
    ...pages.CREATE_ORDER,
    component: () => <CreateOrder />,
  },
  {
    ...pages.ORDER_HISTORY,
    component: () => <OrderHistory />,
  },
  {
    ...pages.ORDER_DETAILS,
    component: () => <OrderDetail />,
  },
  {
    ...pages.MANAGE_WALLET,
    component: () => <ManageWallet />,
  },
  {
    ...pages.LIST_CUSTOMER,
    component: () => <ListCustomer />,
  },
  {
    ...pages.DETAIL_CUSTOMER,
    component: () => <DetailCustomer />,
  },
  {
    ...pages.LIST_SHIPPER,
    component: () => <ListShipper />,
  },
  {
    ...pages.DISCOUNT_CODE,
    component: () => <DiscountCodeList />,
  },
  {
    ...pages.CREATE_DISCOUNT,
    component: () => <CreateDiscount />,
  },
  {
    ...pages.DISCOUNT_CODE_DETAIL,
    component: () => <DiscountCodeDetail />,
  },
];

const renderMenuItem = () =>
  routes.map(route =>
    route.type === routeTypes.PRIVATE ? (
      <PrivateRoute
        key={route.path}
        exact={route.exact}
        path={route.path}
        component={route.component}
      />
    ) : route.type === routeTypes.AUTH ? (
      <AuthRoute
        key={route.path}
        exact={route.exact}
        path={route.path}
        component={route.component}
      />
    ) : (
      <Route
        key={route.path}
        exact={route.exact}
        path={route.path}
        component={route.component}
      />
    )
  );

export default renderMenuItem;
